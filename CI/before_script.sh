#!/usr/bin/env bash

export MAJOR_VERSION=$(head -n 2 PARAMS | tail -1)
export BUILD_NUMBER=$(head -n 4 PARAMS | tail -1)
export PROJECT_NAME=$(head -n 6 PARAMS | tail -1)
export ENVIRONNMENT=$(head -n 8 PARAMS | tail -1)

echo $ENVIRONNMENT

if [ $ENVIRONNMENT == 'Dev' ]; then
  export BUILD_NAME=$PROJECT_NAME'_'$MAJOR_VERSION'.'$BUILD_NUMBER'_'$ENVIRONNMENT;
elif [ $ENVIRONNMENT == 'Rec' ]; then
  export BUILD_NAME=$PROJECT_NAME'_'$MAJOR_VERSION'.'$BUILD_NUMBER'_'$ENVIRONNMENT;
elif [ $ENVIRONNMENT == 'PreProd' ]; then
  export BUILD_NAME=$PROJECT_NAME'_'$MAJOR_VERSION'_'$ENVIRONNMENT;
elif [ $ENVIRONNMENT == 'Prod' ]; then
  export BUILD_NAME=$PROJECT_NAME;
else
  echo "The variable ENVIRONNMENT is not correct";
fi

echo $BUILD_NAME
echo $BUILD_NAME > tempvars
chmod +x ./CI/before_script.sh
mkdir -p /root/.cache/unity3d
mkdir -p /root/.local/share/unity3d/Unity/
echo "$UNITY_LICENSE_CONTENT" > /root/.local/share/unity3d/Unity/Unity_lic.ulf
