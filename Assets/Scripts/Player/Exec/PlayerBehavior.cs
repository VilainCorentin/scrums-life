﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class PlayerBehavior : PlayerSpec
{

    public ManageUI HUD;

    public List<string> listTagScrum;
    public List<string> listTagHarvest;
    public List<string> listTagEssential;
    public List<string> listTagChildScrum;
    public List<string> listTagAll;

    public Vector3 posStrat;
    public bool haveStratPoint;

    private Ray ray;
    private RaycastHit rayHit;

    public ManageChildScrumUi manageChildScrum;
    public ManageHarvestUi manageHarvest;
    public ManageEssentialUi manageEssential;
    public ManageScrumUi manageScrum;
    public ManageViewUi manageView;

    void Start ()
    {
        this.InitPlayer();

        GameObject.Find("Sun").GetComponent<InitGame>().ReadyForLoad();

        this.listTagScrum = new List<string> {"Farmer", "Nurse", "Soldier"};
        this.listTagHarvest = new List<string> {"Food", "Puit"};
        this.listTagEssential = new List<string> { "Granary", "Queen", "Nursery" };
        this.listTagChildScrum = new List<string> { "Egg" };
        this.listTagAll = new List<string>();

        this.listTagAll.AddRange(this.listTagScrum);
        this.listTagAll.AddRange(this.listTagHarvest);
        this.listTagAll.AddRange(this.listTagEssential);
        this.listTagAll.AddRange(this.listTagChildScrum);

        this.manageScrum = new ManageScrumUi(this);
        this.manageHarvest = new ManageHarvestUi(this);
        this.manageEssential = new ManageEssentialUi(this);
        this.manageChildScrum = new ManageChildScrumUi(this);
        this.manageView = new ManageViewUi(this);

        this.posStrat = Vector3.zero;
        this.haveStratPoint = false;

        this.RefresHUD();
    }

    void FixedUpdate ()
    {
        this.data.posx = this.transform.position.x;
        this.data.posy = this.transform.position.y;
        this.data.posz = this.transform.position.z;

        if (this.data.curLife > 0)
        {
            Vector2 screenCenterPoint = new Vector2(Screen.width / 2, Screen.height / 2);

            this.ray = Camera.main.ScreenPointToRay(screenCenterPoint);

            // Manage View
            if (Physics.Raycast(ray, out rayHit, 100f))
            {
                this.manageView.ManageView(rayHit.transform.gameObject);
            }

            // For Scrum
            if (this.manageScrum.onViewScrum != null)
            {
                // Manage Mouse Scrum
                if (Input.GetMouseButtonDown(0))
                {
                    this.manageScrum.ManageMouseScrum();
                }

                // Manage Menu Scrum
                this.manageScrum.ManageMenuScrum();
            }
            // For Harvest
            else if (this.manageHarvest.onViewHarvest != null)
            {
                // Manage Mouse Harvest
                if (Input.GetMouseButtonDown(0))
                {
                    this.manageHarvest.ManageMouseHarvest();
                }

                // Manage Menu Harvest
                this.manageHarvest.ManageMenuHarvest();
            }
            // For Essential Scrum
            else if (this.manageEssential.onViewEssential != null)
            {
                // Manage Mouse Scrum
                if (Input.GetMouseButtonDown(0))
                {
                    this.manageEssential.ManageMouseEssential();
                }

                // Manage Menu Essential
                this.manageEssential.ManageMenuEssential();
            }
            // For Child Scrum
            else if (this.manageChildScrum.onViewChildScrum != null)
            {
                // Manage Menu Child Scrum
                this.manageChildScrum.ManageMenuChildScrum();
            }

            this.manageView.ManageMenuGeneral();
        }
        else
        {
            this.HUD.UpdateTextActionInfo("game over", 0);
            this.HUD.UpdateTextActionInfo("game over", 1);
            this.HUD.UpdateTextActionInfo("game over", 2);
            this.gameObject.GetComponent<CharacterController>().enabled = false;
        }
    }

    public void RefresHUD()
    {
        this.HUD = GameObject.Find("Sun").GetComponent<ManageUI>();

        this.HUD.UpdateTextStockPlayer(this.data.curStockage, this.data.maxStockage);
        this.HUD.UpdateTextFollowerSoldierPlayer(this.followers.Count, this.data.maxFollower);
        this.HUD.UpdateTextLifePlayer(this.data.curLife, this.data.maxLife);
    }

    public void DelFollowers(GameObject soldier)
    {
        if (this.followers.Contains(soldier))
        {
            this.followers.Remove(soldier);
            this.HUD.UpdateTextFollowerSoldierPlayer(this.followers.Count, this.data.maxFollower);
        }
    }
}
