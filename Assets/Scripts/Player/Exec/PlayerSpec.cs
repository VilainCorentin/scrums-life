﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpec : MonoBehaviour
{

    //Manage soldiers
    public List<GameObject> followers;

    //Manage Essential
    public GameObject essentialFollowers;

    //Data
    public DPlayerSpec data;

    public void InitPlayer () 
    {
        this.data = new DPlayerSpec();

        this.data.maxStockage = 30f;
        this.data.maxLife = 300f;

        this.data.curStockage = 0f;
        this.data.curLife = this.data.maxLife;
        
        this.followers = new List<GameObject>();
        this.data.followersSave = new List<DSoldierBehaviour>();
        this.data.maxFollower = 5;

        this.essentialFollowers = null;
    }

}
