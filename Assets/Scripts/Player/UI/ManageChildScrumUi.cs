using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManageChildScrumUi
{
    protected PlayerBehavior pb;

    public GameObject onViewChildScrum;

    public ManageChildScrumUi(PlayerBehavior pb)
    {
        this.pb = pb;
        this.onViewChildScrum = null;
    }

    public void ManageMenuChildScrum()
    {
        //GESTION INFO
        List<string> lstInfo = new List<string>();

        if (this.onViewChildScrum.tag == "Egg")
        {
            lstInfo.Add("Energy : " + this.onViewChildScrum.GetComponent<Egg>().data.curEnergy + "/" + this.onViewChildScrum.GetComponent<Egg>().data.energieToEvolve);

            if (this.onViewChildScrum.GetComponent<Egg>().data.takeCare)
            {
                lstInfo.Add("On care : yes");
            }
            else
            {
                lstInfo.Add("On care : no");
            }

            lstInfo.Add("Time life : " + (int)this.onViewChildScrum.GetComponent<Egg>().data.timeToLive);

            //Gestion action
            this.pb.HUD.UpdateTextAction("Feed", 0);

            if (Input.GetButtonDown("Action1"))
            {
                float valueGiven = this.onViewChildScrum.GetComponent<Egg>().Feed(this.pb.data.curStockage);
                this.pb.data.curStockage = this.pb.data.curStockage - valueGiven;
                this.pb.HUD.UpdateTextStockPlayer(this.pb.data.curStockage, this.pb.data.maxStockage);
            }

        }

        this.pb.HUD.UpdatePanelInfo(this.onViewChildScrum.tag, lstInfo);
    }
}