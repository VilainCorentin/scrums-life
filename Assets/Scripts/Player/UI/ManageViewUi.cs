using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class ManageViewUi
{
    protected PlayerBehavior pb;

    public bool onLock;

    public ManageViewUi(PlayerBehavior pb)
    {
        this.pb = pb;

        this.onLock = false;
    }

    public void ManageView(GameObject obj)
    {
        if(!this.onLock)
        {
            if (this.pb.listTagAll.Contains(obj.tag))
            {
                this.pb.HUD.DisablePlayerYouSee();
                this.pb.HUD.UpdateTextPlayerYouSee(obj.tag);
                this.pb.HUD.DisablePanelInfo();

                if (this.pb.listTagScrum.Contains(obj.tag))
                {
                    this.pb.manageScrum.onViewScrum = obj;
                    this.pb.manageEssential.onViewEssential = null;
                    this.pb.manageHarvest.onViewHarvest = null;
                    this.pb.manageChildScrum.onViewChildScrum = null;
                }
                else if (this.pb.listTagHarvest.Contains(obj.tag))
                {
                    this.pb.manageHarvest.onViewHarvest = obj;
                    this.pb.manageScrum.onViewScrum = null;
                    this.pb.manageEssential.onViewEssential = null;
                    this.pb.manageChildScrum.onViewChildScrum = null;
                }
                else if (this.pb.listTagEssential.Contains(obj.tag))
                {
                    this.pb.manageEssential.onViewEssential = obj;
                    this.pb.manageScrum.onViewScrum = null;
                    this.pb.manageHarvest.onViewHarvest = null;
                    this.pb.manageChildScrum.onViewChildScrum = null;
                }
                else
                {
                    this.pb.manageChildScrum.onViewChildScrum = obj;
                    this.pb.manageEssential.onViewEssential = null;
                    this.pb.manageScrum.onViewScrum = null;
                    this.pb.manageHarvest.onViewHarvest = null;
                }
            }
            else
            {
                if (this.pb.HUD.textYouSee.enabled)
                {
                    this.pb.HUD.DisablePlayerYouSee();
                    this.pb.HUD.DisablePanelInfo();

                    this.pb.manageScrum.onViewScrum = null;
                    this.pb.manageHarvest.onViewHarvest = null;
                    this.pb.manageEssential.onViewEssential = null;
                    this.pb.manageChildScrum.onViewChildScrum = null;
                }
            }
        }
    }

    public void ManageMenuGeneral()
    {

        if (Input.GetKeyDown(KeyCode.Return))
        {
            this.pb.HUD.textInfoStart.SetActive(false);
        }

        if (this.pb.essentialFollowers != null)
        {

            Vector3 home = this.pb.essentialFollowers.GetComponent<EssentialBehaviour>().home;
            this.pb.essentialFollowers.GetComponent<EssentialBehaviour>().RefreshHomePos();

            if (Vector3.Distance(home, pb.transform.position) > 50)
            {
                this.pb.HUD.UpdateTextFollowerEssentialPlayer(this.pb.essentialFollowers.tag + "(far)");
            }
            else
            {
                this.pb.HUD.UpdateTextFollowerEssentialPlayer(this.pb.essentialFollowers.tag);

                if (Input.GetButtonDown("Action4"))
                {
                    this.pb.essentialFollowers.GetComponent<EssentialBehaviour>().home = pb.transform.position;
                    this.pb.essentialFollowers.GetComponent<EssentialBehaviour>().RefreshHomePos();
                    this.pb.essentialFollowers.GetComponent<EssentialBehaviour>().agent.SetDestination(pb.transform.position);
                    this.pb.essentialFollowers.GetComponent<EssentialBehaviour>().follower = null;
                    this.pb.essentialFollowers = null;
                    this.pb.HUD.DisableTextFollowerEssentialPlayer();
                }
            }
        }

        if (Input.GetButtonDown("MyActions"))
        {
            this.pb.HUD.UpdateMyActions();
        }

        if (Input.GetButtonDown("ConvertLife"))
        {
            float tmp_value = Mathf.Clamp(pb.data.curStockage, 0, pb.data.maxLife - pb.data.curLife);

            pb.data.curLife = pb.data.curLife + tmp_value;
            pb.data.curStockage = pb.data.curStockage - tmp_value;

            this.pb.HUD.UpdateTextLifePlayer(pb.data.curLife, pb.data.maxLife);
            this.pb.HUD.UpdateTextStockPlayer(pb.data.curStockage, pb.data.maxStockage);
        }

        if (Input.GetButtonDown("Save"))
        {
            GameObject.Find("Sun").GetComponent<InitGame>().Save();
        }
        
        if (this.onLock && Input.GetMouseButtonDown(1))
        {
            this.onLock = false;

            this.pb.HUD.DisablePlayerYouSee();
            this.pb.HUD.DisablePanelInfo();

            if (this.pb.manageScrum.onViewScrum != null)
            {
                if (this.pb.manageScrum.onViewScrum.tag == "Farmer")
                {
                    this.pb.manageScrum.onViewScrum.GetComponent<FarmerBehaviour>().manageObjectiveSpecific.ResetObjective();
                }
                else if (this.pb.manageScrum.onViewScrum.tag == "Nurse")
                {
                    this.pb.manageScrum.onViewScrum.GetComponent<NurseBehaviour>().manageObjectiveSpecific.ResetObjective();
                }
                else if (this.pb.manageScrum.onViewScrum.tag == "Soldier")
                {
                    this.pb.manageScrum.onViewScrum.GetComponent<SoldierBehaviour>().manageObjectiveSpecific.ResetObjective();
                }

                this.pb.manageScrum.onViewScrum.transform.Find("DotLightLock").gameObject.SetActive(false);
            }

            this.pb.manageScrum.onViewScrum = null;
            this.pb.manageHarvest.onViewHarvest = null;
            this.pb.manageEssential.onViewEssential = null;
            this.pb.manageChildScrum.onViewChildScrum = null;
        }
    }
}