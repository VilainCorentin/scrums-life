using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManageHarvestUi
{
    protected PlayerBehavior pb;

    public GameObject onViewHarvest;

    public ManageHarvestUi(PlayerBehavior pb)
    {
        this.pb = pb;
        this.onViewHarvest = null;
    }

    public void ManageMenuHarvest()
    {
        //GESTION INFO
        List<string> lstInfo = new List<string>();
        string panelTitle = "";

        if (this.onViewHarvest.tag == "Food")
        {
            panelTitle = "Food";
            float foodStock = this.onViewHarvest.GetComponent<Foodspec>().foodPower;
            lstInfo.Add("Stock : " + foodStock);

            this.pb.HUD.UpdateTextActionInfo("Click to harvest", 2);
        }
        else if (this.onViewHarvest.tag == "Puit")
        {
            panelTitle = "Strategic Point";
            lstInfo.Add("Type : Food");

            if (this.pb.haveStratPoint)
            {
                this.pb.HUD.UpdateTextActionInfo("You have already a strategic point", 2);
            }
            else
            {
                this.pb.HUD.UpdateTextActionInfo("Click to take the strategic point", 2);
            }
        }

        this.pb.HUD.UpdatePanelInfo(panelTitle, lstInfo);
    }

    public void ManageMouseHarvest()
    {
        if (this.onViewHarvest.tag == "Food")
        {
            float valueToHarvest = this.pb.data.maxStockage - this.pb.data.curStockage;
            this.pb.data.curStockage = this.pb.data.curStockage + this.onViewHarvest.GetComponent<Foodspec>().Harvest(valueToHarvest);
            this.pb.HUD.UpdateTextStockPlayer(this.pb.data.curStockage, this.pb.data.maxStockage);
        }
        else if (this.onViewHarvest.tag == "Puit")
        {
            if (!this.pb.haveStratPoint)
            {
                this.pb.posStrat = this.onViewHarvest.transform.position;
                this.pb.haveStratPoint = true;
            }
        }
    }
}