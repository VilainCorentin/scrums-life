using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManageEssentialUi
{
    protected PlayerBehavior pb;

    public GameObject onViewEssential;

    public ManageEssentialUi(PlayerBehavior pb)
    {
        this.pb = pb;
        this.onViewEssential = null;
    }

    public void ManageMenuEssential()
    {
        List<string> lstInfo = new List<string>();

        if (this.pb.essentialFollowers == null)
        {
            this.pb.HUD.UpdateTextAction("Follow me", 2);

            if (Input.GetButtonDown("Action3"))
            {
                this.pb.essentialFollowers = this.onViewEssential;
                this.pb.HUD.UpdateTextFollowerEssentialPlayer(this.onViewEssential.tag);
                this.onViewEssential.GetComponent<EssentialBehaviour>().follower = pb.gameObject;
            }
        }

        if (this.onViewEssential.tag == "Queen")
        {
            RessourceQueenPlayer ressouce = this.onViewEssential.GetComponent<QueenBehaviour>().ressource as RessourceQueenPlayer;

            lstInfo.Add("Energy : " + this.onViewEssential.GetComponent<QueenBehaviour>().data.curEnergy);
            lstInfo.Add("Farmer : " + ressouce.totalFarmer);
            lstInfo.Add("Nurse : " + ressouce.totalNurse);
            lstInfo.Add("Soldier : " + ressouce.totalSoldier);

            this.pb.HUD.UpdateTextAction("Feed", 0);

            if (Input.GetButtonDown("Action1"))
            {
                float valueGiven = this.onViewEssential.GetComponent<QueenBehaviour>().Feed(this.pb.data.curStockage);
                this.pb.data.curStockage = this.pb.data.curStockage - valueGiven;
                this.pb.HUD.UpdateTextStockPlayer(this.pb.data.curStockage, this.pb.data.maxStockage);
            }
        }
        else if (this.onViewEssential.tag == "Granary")
        {
            lstInfo.Add("Stock : " + this.onViewEssential.GetComponent<GranarySoul>().data.howManyFood + "/" + this.onViewEssential.GetComponent<GranarySoul>().data.maxFood);

            this.pb.HUD.UpdateTextAction("Stock food", 0);

            if (Input.GetButtonDown("Action1"))
            {
                float tmpFood = this.onViewEssential.GetComponent<GranarySoul>().EnterFood(this.pb.data.curStockage);
                this.pb.data.curStockage = this.pb.data.curStockage - tmpFood;
                this.pb.HUD.UpdateTextStockPlayer(this.pb.data.curStockage, this.pb.data.maxStockage);
            }

            this.pb.HUD.UpdateTextAction("take food", 1);

            if (Input.GetButtonDown("Action2"))
            {
                float tmpFood = this.onViewEssential.GetComponent<GranarySoul>().OutFood(this.pb.data.maxStockage - this.pb.data.curStockage);
                this.pb.data.curStockage = this.pb.data.curStockage + tmpFood;
                this.pb.HUD.UpdateTextStockPlayer(this.pb.data.curStockage, this.pb.data.maxStockage);
            }
        }

        this.pb.HUD.UpdatePanelInfo(this.onViewEssential.tag, lstInfo);
    }

    public void ManageMouseEssential()
    {
        this.pb.manageView.onLock = true;
    }
}