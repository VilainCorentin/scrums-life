using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManageScrumUi
{
    protected PlayerBehavior pb;

    public GameObject onViewScrum;

    public ManageScrumUi(PlayerBehavior pb)
    {
        this.pb = pb;
        this.onViewScrum = null;
    }

    public void ManageMenuScrum()
    {
        QueenBehaviour queen = this.onViewScrum.GetComponent<ScrumBehavior>().myQueen.GetComponent<QueenBehaviour>();

        if (queen.data.numberID == 0)
        {
            List<string> lstInfo = new List<string>();

            lstInfo.Add("Energy : " + this.onViewScrum.GetComponent<ScrumBehavior>().pData.curEnergy + "/" + this.onViewScrum.GetComponent<ScrumBehavior>().pData.maxEnergie);
            lstInfo.Add("Life : " + this.onViewScrum.GetComponent<ScrumBehavior>().pData.curLife + "/" + this.onViewScrum.GetComponent<ScrumBehavior>().pData.maxLife);
            lstInfo.Add("Stockage : " + this.onViewScrum.GetComponent<ScrumBehavior>().pData.curStockage + "/" + this.onViewScrum.GetComponent<ScrumBehavior>().pData.stockage);
            lstInfo.Add(PlayerToolsUi.ObjectifToString(this.onViewScrum.GetComponent<ScrumBehavior>().pData.typeObjective));

            if (this.onViewScrum.tag == "Farmer")
            {
                if (this.onViewScrum.GetComponent<FarmerBehaviour>().data.nbDiffPuit > 0)
                {
                    lstInfo.Add("Stragic point : yes");
                }
                else
                {
                    lstInfo.Add("Stragic point : no");
                }

                this.pb.HUD.UpdateTextAction("Feed", 0);

                if (Input.GetButtonDown("Action1"))
                {
                    float valueGiven = this.onViewScrum.GetComponent<FarmerBehaviour>().Feed(this.pb.data.curStockage);
                    this.pb.data.curStockage = this.pb.data.curStockage - valueGiven;
                    this.pb.HUD.UpdateTextStockPlayer(this.pb.data.curStockage, this.pb.data.maxStockage);
                }

                if (this.pb.haveStratPoint)
                {
                    this.pb.HUD.UpdateTextAction("Spread strategic point", 2);

                    if (Input.GetButtonDown("Action3"))
                    {
                        this.onViewScrum.GetComponent<FarmerBehaviour>().ReceiveStrategicPoint(this.pb.posStrat);
                        this.pb.haveStratPoint = false;
                        this.pb.posStrat = Vector3.zero;
                    }
                }
            }
            else if (this.onViewScrum.tag == "Nurse")
            {
                this.pb.HUD.UpdateTextAction("Go pick egg", 0);
                this.pb.HUD.UpdateTextAction("Feed", 1);

                if (Input.GetButtonDown("Action1"))
                {
                    Vector3 pos = this.onViewScrum.GetComponent<NurseBehaviour>().myQueen.transform.position;
                    this.onViewScrum.GetComponent<NurseBehaviour>().manageMove.SetDirection(pos);
                }
                else if (Input.GetButtonDown("Action2"))
                {
                    float valueGiven = this.onViewScrum.GetComponent<NurseBehaviour>().Feed(this.pb.data.curStockage);
                    this.pb.data.curStockage = this.pb.data.curStockage - valueGiven;
                    this.pb.HUD.UpdateTextStockPlayer(this.pb.data.curStockage, this.pb.data.maxStockage);
                }
            }
            else if (this.onViewScrum.tag == "Soldier")
            {
                // GESTION INFO
                if (this.onViewScrum.GetComponent<SoldierBehaviour>().data.isLeader)
                {
                    lstInfo.Add("Leader : Yes");
                    lstInfo.Add("Followers : " + this.onViewScrum.GetComponent<SoldierBehaviour>().followers.Count + "/" + this.onViewScrum.GetComponent<SoldierBehaviour>().data.maxFollower);
                }
                else
                {
                    lstInfo.Add("Leader : No");

                    if (this.onViewScrum.GetComponent<SoldierBehaviour>().haveALeader != null)
                    {
                        lstInfo.Add("Have a leader : Yes");
                    }
                    else
                    {
                        lstInfo.Add("Have a leader : No");
                    }
                }

                // GESTION MENU
                this.pb.HUD.UpdateTextAction("Go defend the queen", 1);

                if (!this.pb.followers.Contains(this.onViewScrum) && this.pb.followers.Count < this.pb.data.maxFollower)
                {
                    this.pb.HUD.UpdateTextAction("Follow me", 0);

                    if (Input.GetButtonDown("Action1"))
                    {
                        this.onViewScrum.GetComponent<SoldierBehaviour>().haveALeader = pb.gameObject;
                        this.onViewScrum.GetComponent<SoldierBehaviour>().data.leaderIsPlayer = true;
                        this.pb.followers.Add(this.onViewScrum);
                        this.pb.HUD.UpdateTextFollowerSoldierPlayer(this.pb.followers.Count, this.pb.data.maxFollower);
                    }
                }

                if (Input.GetButtonDown("Action2"))
                {
                    this.onViewScrum.GetComponent<SoldierBehaviour>().manageMove.SetDirection(this.onViewScrum.GetComponent<SoldierBehaviour>().myQueen);

                    this.onViewScrum.GetComponent<SoldierBehaviour>().pointToDefend = Vector3.zero;
                    this.onViewScrum.GetComponent<SoldierBehaviour>().RefreshPosPointToDefend();

                    if (this.pb.followers.Contains(this.onViewScrum))
                    {
                        this.onViewScrum.GetComponent<SoldierBehaviour>().haveALeader = null;
                        this.onViewScrum.GetComponent<SoldierBehaviour>().data.leaderIsPlayer = false;
                        this.pb.followers.Remove(this.onViewScrum);
                        this.pb.HUD.UpdateTextFollowerSoldierPlayer(this.pb.followers.Count, this.pb.data.maxFollower);
                    }
                }

                if (this.pb.followers.Count > 0)
                {
                    bool checkMySoldier = false;

                    for (int i = 0; i < this.pb.followers.Count; i++)
                    {
                        if (this.pb.followers[i].GetComponent<ScrumBehavior>().pData.identify == this.onViewScrum.GetComponent<ScrumBehavior>().pData.identify)
                        {
                            checkMySoldier = true;
                            break;
                        }
                    }

                    if (checkMySoldier)
                    {
                        this.pb.HUD.UpdateTextAction("Defend this area", 2);

                        if (Input.GetButtonDown("Action3"))
                        {
                            for (int i = 0; i < this.pb.followers.Count; ++i)
                            {
                                this.pb.followers[i].GetComponent<SoldierBehaviour>().pointToDefend = pb.transform.position;
                                this.pb.followers[i].GetComponent<SoldierBehaviour>().RefreshPosPointToDefend();
                                this.pb.followers[i].GetComponent<SoldierBehaviour>().haveALeader = null;
                                this.onViewScrum.GetComponent<SoldierBehaviour>().data.leaderIsPlayer = false;
                                this.pb.followers[i].GetComponent<SoldierBehaviour>().agent.stoppingDistance = 0;
                            }

                            this.pb.followers.Clear();
                            this.pb.HUD.UpdateTextFollowerSoldierPlayer(this.pb.followers.Count, this.pb.data.maxFollower);
                        }
                    }
                }
            }

            string title = this.onViewScrum.tag;

            if(this.pb.manageView.onLock)
            {
                title = title + " (lock)";
            }

            this.pb.HUD.UpdatePanelInfo(title, lstInfo);
        }
        else
        {
            this.pb.HUD.UpdateTextActionInfo("enemy", 2, Color.red);

            if (this.pb.followers.Count > 0)
            {
                this.pb.HUD.UpdateTextAction("Order to attack", 0);

                if (Input.GetButtonDown("Action1"))
                {
                    for (int i = 0; i < this.pb.followers.Count; ++i)
                    {
                        this.pb.followers[i].GetComponent<SoldierBehaviour>().prey = this.onViewScrum;
                        this.pb.followers[i].GetComponent<SoldierBehaviour>().pData.typeObjective = (int)ObjectifTypeEnum.GO_ATTACK;
                    }
                }
            }
        }

    }

    public void ManageMouseScrum()
    {
        QueenBehaviour queen = this.onViewScrum.GetComponent<ScrumBehavior>().myQueen.GetComponent<QueenBehaviour>();

        if (queen.data.numberID == 0)
        {
            this.onViewScrum.transform.Find("DotLightLock").gameObject.SetActive(true);

            if (this.onViewScrum.tag == "Farmer")
            {
                this.onViewScrum.GetComponent<FarmerBehaviour>().pData.typeObjective = (int)ObjectifTypeEnum.IDLE_PLAYER;
                this.onViewScrum.GetComponent<ScrumBehavior>().agent.SetDestination(this.onViewScrum.transform.position);
                this.pb.manageView.onLock = true;
            }
            else if (this.onViewScrum.tag == "Nurse")
            {
                this.onViewScrum.GetComponent<NurseBehaviour>().pData.typeObjective = (int)ObjectifTypeEnum.IDLE_PLAYER;
                this.onViewScrum.GetComponent<ScrumBehavior>().agent.SetDestination(this.onViewScrum.transform.position);
                this.pb.manageView.onLock = true;
            }
            else if (this.onViewScrum.tag == "Soldier")
            {
                this.onViewScrum.GetComponent<SoldierBehaviour>().pData.typeObjective = (int)ObjectifTypeEnum.IDLE_PLAYER;
                this.onViewScrum.GetComponent<ScrumBehavior>().agent.SetDestination(this.onViewScrum.transform.position);
                this.pb.manageView.onLock = true;
            }
        }
    }
}