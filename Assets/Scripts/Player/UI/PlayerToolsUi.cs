public static class PlayerToolsUi
{
    public static string ObjectifToString(int ind)
    {
        switch (ind)
        {
            case 0:
                return "Thinks";
            case 1:
                return "Pick Food";
            case 2:
                return "Go to Granary";
            case 3:
                return "Take food for Queen";
            case 4:
                return "Take care of Queen";
            case 5:
                return "Take care of Egg";
            case 6:
                return "Transport Egg";
            case 7:
                return "Feed Egg";
            case 8:
                return "Communication";
            case 9:
                return "Go to Strategic Point";
            case 10:
                return "Dead";
            case 11:
                return "On Attack";
            case 12:
                return "Wait for order";
            default:
                return "";
        }
    }
}