﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

[Serializable]
public class DPlayerSpec
{
    // Manage stockage
    public float curStockage;
    public float maxStockage;

    // Manage life point
    public float curLife;
    public float maxLife;

    //Manage soldiers
    public List<DSoldierBehaviour> followersSave;
    public int maxFollower;

    //Position
    public float posx;
    public float posy;
    public float posz;
}