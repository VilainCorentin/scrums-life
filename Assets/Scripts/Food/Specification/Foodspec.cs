﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Foodspec : MonoBehaviour
{

    public float foodPower;

    void Update()
    {

        if (foodPower <= 0)
        {
            Destroy(gameObject);
        }

    }

    public float Harvest(float value)
    {
        float valueReturn = Mathf.Clamp(value, 0, foodPower);
        foodPower = foodPower - valueReturn;
        return valueReturn;
    }

}
