﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerFood : MonoBehaviour
{

    //Array of foods
    public GameObject[] foodReserve = new GameObject[5];
    int taille = 5;

    //Manage random
    private float spawnTime;
    private float nextSpawnTime;
    private int randomIndex;
    private float timerMin;
    private float timerMax;

    void Start()
    {
        //Timer
        timerMin = 60f;
        timerMax = 240f;
        NextTimeSpawn();

        //Random Spawn
        randomIndex = (int)Random.Range(0f, (float)taille - 1);

        //Manage parent (if he have)
        if (transform.parent != null)
        {
            transform.position = transform.parent.position;
        }
    }

    void Update()
    {

        if (Time.time > spawnTime)
        {
            NextTimeSpawn();
            Instantiate(foodReserve[randomIndex], transform.position, Quaternion.identity);
            randomIndex = (int)Random.Range(0f, (float)taille - 1);
        }

    }

    private void NextTimeSpawn()
    {
        nextSpawnTime = Random.Range(timerMin, timerMax);
        spawnTime = Time.time + nextSpawnTime;
    }
}
