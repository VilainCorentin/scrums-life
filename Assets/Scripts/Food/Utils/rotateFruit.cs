﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rotateFruit : MonoBehaviour
{

    private Vector3 rotateV;

    void Start()
    {
        rotateV.Set(0, 1, 0);
    }

    void FixedUpdate()
    {
        //Rotate for style
        transform.Rotate(rotateV / 3);
    }
}
