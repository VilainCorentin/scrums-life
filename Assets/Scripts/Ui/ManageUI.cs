﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ManageUI : MonoBehaviour
{
    public Color mainColor;

    public float population;
    public float stockFood;

    public Text textPopulation;
    public Text textStockFood;
    public Text textLife;

    public Text textYouSee;
    public Text textAction1;
    public Text textAction2;
    public Text textAction3;

    public Text textCurStock;

    public Text textFollowerSoldier;
    public Text textFollowerEssential;

    public string textPopulationBase;
    public string textStockFoodBase;

    public string textYouSeeTxt;
    public string textAction1txt;
    public string textAction2txt;
    public string textAction3txt;

    public string textCurStocktxt;
    public string textLifetxt;

    public string textFollowerSoldiertxt;
    public string textFollowerEssentialtxt;

    public List<string> lst_textActiontxt;
    public List<Text> lst_textAction;
    
    // TODO FAIRE UNE CLASSE POUR TOUT CA
    public Image panelInfo;
    public Text panelTextInfo;

    public Text panelTextAtt0;
    public Text panelTextAtt1;
    public Text panelTextAtt2;
    public Text panelTextAtt3;
    public Text panelTextAtt4;
    public Text panelTextAtt5;
    public Text panelTextAtt6;

    public List<Text> lst_textPanel;

    public Image panelMyActionq;
    public Text panelTitleMyActions;

    public Text panelMyActionsTextAtt0;
    public Text panelMyActionsTextAtt1;

    public List<Text> lst_textMyActions;

    public GameObject textInfoStart;

    void Awake()
    {
        mainColor = textYouSee.color;

        population = 0;
        stockFood = 0;

        textPopulationBase = "Population : ";
        textStockFoodBase = "Food : ";

        textYouSeeTxt = "You see : ";

        textYouSee.enabled = false;

        textAction1txt = "Press A\n";
        textAction2txt = "Press E\n";
        textAction3txt = "Press F\n";

        textCurStocktxt = "Stock : ";
        textLifetxt = "Life : ";

        textFollowerSoldiertxt = "Soldiers : ";
        textFollowerEssentialtxt = " follow you\n(Press P for drop)";

        DisableTextFollowerEssentialPlayer();

        lst_textActiontxt = new List<string> { textAction1txt, textAction2txt, textAction3txt };
        lst_textAction = new List<Text> { textAction1, textAction2, textAction3 };
        lst_textPanel = new List<Text> { panelTextAtt0, panelTextAtt1, panelTextAtt2, panelTextAtt3, panelTextAtt4, panelTextAtt5, panelTextAtt6 };
        lst_textMyActions = new List<Text> { panelMyActionsTextAtt0, panelMyActionsTextAtt1 };

        for (int i = 0; i < lst_textAction.Count; i++)
        {
            lst_textAction[i].enabled = false;
        }

        for (int i = 0; i < lst_textPanel.Count; i++)
        {
            lst_textPanel[i].enabled = false;
        }

        for (int i = 0; i < lst_textMyActions.Count; i++)
        {
            lst_textMyActions[i].enabled = false;
        }

        panelInfo.enabled = false;
        panelTextInfo.enabled = false;

        panelMyActionq.enabled = false;
        panelTitleMyActions.enabled = false;

        UpdateText();
    }

    public void IncreasePopulation()
    {
        population = population + 1;
        UpdateText();
    }

    public void DecreasePopulation()
    {
        population = population - 1;
        UpdateText();
    }

    public void SetFood(float food)
    {
        stockFood = food;
        UpdateText();
    }

    public void UpdateText()
    {
        textPopulation.text = textPopulationBase + population;
        textStockFood.text = textStockFoodBase + stockFood;
    }

    public void UpdateMyActions()
    {
        if (!panelMyActionq.enabled)
        {
            for (int i = 0; i < lst_textMyActions.Count; i++)
            {
                lst_textMyActions[i].enabled = true;
            }

            panelMyActionq.enabled = true;
            panelTitleMyActions.enabled = true;
        }
        else
        {
            DisableMyActions();
        }
    }

    public void DisableMyActions()
    {
        panelMyActionq.enabled = false;
        panelTitleMyActions.enabled = false;

        for (int i = 0; i < lst_textMyActions.Count; i++)
        {
            lst_textMyActions[i].enabled = false;
        }
    }

    public void UpdatePanelInfo(string title, List<string> lstAttibut)
    {
        panelTextInfo.text = title;

        for (int i = 0 ; i < lstAttibut.Count ; i++)
        {
            lst_textPanel[i].text = lstAttibut[i];
            lst_textPanel[i].enabled = true;
        }

        if (!panelInfo.enabled)
        {
            panelInfo.enabled = true;
            panelTextInfo.enabled = true;
        }
    }

    public void DisablePanelInfo()
    {
        if (panelInfo.enabled)
        {
            panelInfo.enabled = false;
            panelTextInfo.enabled = false;

            for (int i = 0; i < lst_textPanel.Count; i++)
            {
                lst_textPanel[i].enabled = false;
            }
        }
    }

    public void UpdateTextPlayerYouSee(string tag)
    {
        if (!textYouSee.enabled)
        {
            textYouSee.text = textYouSeeTxt + tag;
            textYouSee.enabled = true;
        }
    }

    public void UpdateTextAction(string text, int ind)
    {
        if (!lst_textAction[ind].enabled)
        {
            lst_textAction[ind].color = mainColor;
            lst_textAction[ind].text = lst_textActiontxt[ind] + text;
            lst_textAction[ind].enabled = true;
        }
    }

    public void UpdateTextActionInfo(string text, int ind)
    {
        if (!lst_textAction[ind].enabled || lst_textAction[ind].text != text)
        {
            lst_textAction[ind].color = mainColor;
            lst_textAction[ind].text = text;
            lst_textAction[ind].enabled = true;
        }
    }

    public void UpdateTextActionInfo(string text, int ind, Color color)
    {
        if (!lst_textAction[ind].enabled || lst_textAction[ind].text != text)
        {
            lst_textAction[ind].text = text;
            lst_textAction[ind].color = color;
            lst_textAction[ind].enabled = true;
        }
    }

    public void DisablePlayerYouSee()
    {
        if (textYouSee.enabled)
        {
            textYouSee.enabled = false;

            RefreshMenuPlayer();
        }
    }

    public void RefreshMenuPlayer()
    {
        for (int i = 0; i < lst_textAction.Count; i++)
        {
            lst_textAction[i].enabled = false;
        }
    }

    public void UpdateTextStockPlayer(float stock, float max)
    {
        textCurStock.text = textCurStocktxt + stock + " / " + max;
    }

    public void UpdateTextLifePlayer(float life, float max)
    {
        textLife.text = textLifetxt + life + " / " + max;
    }

    public void UpdateTextFollowerSoldierPlayer(float current, float max)
    {
        textFollowerSoldier.text = textFollowerSoldiertxt + current + " / " + max;
    }

    public void UpdateTextFollowerEssentialPlayer(string text)
    {
        textFollowerEssential.text = text + textFollowerEssentialtxt;
    }

    public void DisableTextFollowerEssentialPlayer()
    {
        textFollowerEssential.text = "My actions (U)";
    }
}
