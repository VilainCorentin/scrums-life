﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class SaveGame {

    public void Save(List<GameObject> lst_allQueen)
    {
        GeneralSave generalSave = new GeneralSave();

        //Save Player
        GameObject.Find("Player").GetComponent<PlayerSpec>().data.posx = GameObject.Find("Player").transform.position.x;
        GameObject.Find("Player").GetComponent<PlayerSpec>().data.posy = GameObject.Find("Player").transform.position.y;
        GameObject.Find("Player").GetComponent<PlayerSpec>().data.posz = GameObject.Find("Player").transform.position.z;
        generalSave.player = GameObject.Find("Player").GetComponent<PlayerSpec>().data;

        //Game Identity
        generalSave.currentIdentity = GameObject.Find("Sun").GetComponent<InitGame>().currentIdentify;

        Debug.Log("GRANARY : " + lst_allQueen[0].GetComponent<QueenBehaviour>().data.granaryData.dataParent.posHomex);

        // Save Queen
        for (int i = 0; i < lst_allQueen.Count; i++)
        {

            generalSave.lst_allQueen.Add(lst_allQueen[i].GetComponent<QueenBehaviour>().data);

        }

        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/gamesave.save");
        bf.Serialize(file, generalSave);
        file.Close();
        Debug.Log("SAVE");
    }

}
