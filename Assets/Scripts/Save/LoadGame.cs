﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class LoadGame : MonoBehaviour
{

    public GameObject farmerPref;
    public GameObject nursePref;
    public GameObject soldierPref;

    public void Load(List<GameObject> lst_allQueen)
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Open(Application.persistentDataPath + "/gamesave.save", FileMode.Open);
        GeneralSave generalSave = (GeneralSave)bf.Deserialize(file);
        file.Close();

        // Player
        GameObject.Find("Player").GetComponent<PlayerSpec>().data = generalSave.player;
        Vector3 posPlayer = new Vector3(generalSave.player.posx, generalSave.player.posy, generalSave.player.posz);
        GameObject.Find("Player").transform.position = posPlayer;

        GameObject.Find("Player").GetComponent<PlayerBehavior>().RefresHUD();

        for (int i = 0; i < lst_allQueen.Count; i++)
        {

            // Init Queen
            lst_allQueen[i].GetComponent<QueenBehaviour>().data = generalSave.lst_allQueen[i];
            lst_allQueen[i].GetComponent<QueenBehaviour>().ressource = generalSave.lst_allQueen[i].ressourceSave;

            Vector3 posQueen = new Vector3(generalSave.lst_allQueen[i].dataParent.posHomex, generalSave.lst_allQueen[i].dataParent.posHomey, generalSave.lst_allQueen[i].dataParent.posHomez);
            lst_allQueen[i].transform.position = posQueen;

            // Queen Player
            if (i == 0)
            {
                GameObject.Find("Sun").GetComponent<ManageUI>().SetFood(generalSave.lst_allQueen[i].ressourceSave.getTotalFood());
                GameObject.Find("Sun").GetComponent<ManageUI>().population = generalSave.lst_allQueen[i].ressourceSave.getTotalPop();
            }

            // Init Granary
            lst_allQueen[i].GetComponent<QueenBehaviour>().granarySoul.GetComponent<GranarySoul>().data = generalSave.lst_allQueen[i].granaryData;

            Debug.Log("SECOND");

            Vector3 posGranary = new Vector3(generalSave.lst_allQueen[i].granaryData.dataParent.posHomex, generalSave.lst_allQueen[i].granaryData.dataParent.posHomey, generalSave.lst_allQueen[i].granaryData.dataParent.posHomez);
            lst_allQueen[i].GetComponent<QueenBehaviour>().granarySoul.transform.position = posGranary;

            // Init Nursery
            //Todo

            List<DFarmerBehaviour> lst_Farmer = generalSave.lst_allQueen[i].ressourceSave.getLstFarmer();

            for (int y = 0; y < lst_Farmer.Count; y++)
            {
                GameObject farmer = Instantiate(this.farmerPref, new Vector3(lst_Farmer[y].dataParent.posx, lst_Farmer[y].dataParent.posy, lst_Farmer[y].dataParent.posz), Quaternion.identity) as GameObject;

                farmer.GetComponent<FarmerBehaviour>().pData = lst_Farmer[y].dataParent;
                farmer.GetComponent<FarmerBehaviour>().data = lst_Farmer[y];

                farmer.GetComponent<ScrumBehavior>().myQueen = lst_allQueen[i];
                farmer.GetComponent<ScrumBehavior>().myGranary = lst_allQueen[i].GetComponent<QueenBehaviour>().granarySoul;
                farmer.GetComponent<ScrumBehavior>().myNursery = lst_allQueen[i].GetComponent<QueenBehaviour>().nurserySoul;

                if (lst_allQueen[i].GetComponent<QueenBehaviour>().data.numberID != 0)
                {
                    farmer.transform.Find("DotLightEnn").gameObject.SetActive(true);
                }

                Debug.Log("FARMER LOAD");

            }

            List<DNurseBehaviour> lst_nurse = generalSave.lst_allQueen[i].ressourceSave.getLstNurse();

            for (int y = 0; y < lst_nurse.Count; y++)
            {
                GameObject nurse = Instantiate(this.nursePref, new Vector3(lst_nurse[y].dataParent.posx, lst_nurse[y].dataParent.posy, lst_nurse[y].dataParent.posz), Quaternion.identity) as GameObject;

                nurse.GetComponent<NurseBehaviour>().pData = lst_nurse[y].dataParent;
                nurse.GetComponent<NurseBehaviour>().data = lst_nurse[y];

                nurse.GetComponent<ScrumBehavior>().myQueen = lst_allQueen[i];
                nurse.GetComponent<ScrumBehavior>().myGranary = lst_allQueen[i].GetComponent<QueenBehaviour>().granarySoul;
                nurse.GetComponent<ScrumBehavior>().myNursery = lst_allQueen[i].GetComponent<QueenBehaviour>().nurserySoul;

                if (lst_allQueen[i].GetComponent<QueenBehaviour>().data.numberID != 0)
                {
                    nurse.transform.Find("DotLightEnn").gameObject.SetActive(true);
                }

                Debug.Log("NURSE LOAD");

            }

            List<DSoldierBehaviour> lst_soldier = generalSave.lst_allQueen[i].ressourceSave.getLstSoldier();

            for (int y = 0; y < lst_soldier.Count; y++)
            {
                GameObject soldier = Instantiate(this.soldierPref, new Vector3(lst_soldier[y].dataParent.posx, lst_soldier[y].dataParent.posy, lst_soldier[y].dataParent.posz), Quaternion.identity) as GameObject;

                soldier.GetComponent<SoldierBehaviour>().pData = lst_soldier[y].dataParent;
                soldier.GetComponent<SoldierBehaviour>().data = lst_soldier[y];

                soldier.GetComponent<ScrumBehavior>().myQueen = lst_allQueen[i];
                soldier.GetComponent<ScrumBehavior>().myGranary = lst_allQueen[i].GetComponent<QueenBehaviour>().granarySoul;
                soldier.GetComponent<ScrumBehavior>().myNursery = lst_allQueen[i].GetComponent<QueenBehaviour>().nurserySoul;

                if (lst_allQueen[i].GetComponent<QueenBehaviour>().data.numberID != 0)
                {
                    soldier.transform.Find("DotLightEnn").gameObject.SetActive(true);
                }

                Debug.Log("SOLDIER LOAD");

            }
        }

        GameObject.Find("Sun").GetComponent<ManageUI>().UpdateText();

        Debug.Log("GAME LOAD");
    }
  

}
