﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrumSense : MonoBehaviour
{

    public ISense manageSense;
    public UnityEngine.AI.NavMeshAgent agent;

    void Awake()
    {

        if (transform.parent.GetComponent<FarmerBehaviour>() != null)
        {
            this.manageSense = transform.parent.GetComponent<FarmerBehaviour>().manageSense;
        }
        else if (transform.parent.GetComponent<NurseBehaviour>() != null)
        {
            this.manageSense = transform.parent.GetComponent<NurseBehaviour>().manageSense;
        }
        else if (transform.parent.GetComponent<SoldierBehaviour>() != null)
        {
            this.manageSense = transform.parent.GetComponent<SoldierBehaviour>().manageSense;
        }

        this.agent = transform.parent.GetComponent<UnityEngine.AI.NavMeshAgent>();
    }

}
