﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrumView : ScrumSense
{

    void OnTriggerEnter(Collider other)
    {
        if (this.agent.enabled)
        {
            this.manageSense.FindByView(other.gameObject);
        }
    }
}
