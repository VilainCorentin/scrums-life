﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrumCac : ScrumSense
{

    void OnTriggerEnter(Collider other)
    {
        if (this.agent.enabled)
        {
            this.manageSense.FindByCac(other.gameObject);
        }
    }
}
