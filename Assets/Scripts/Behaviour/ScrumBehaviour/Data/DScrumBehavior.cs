﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class DScrumBehavior
{
    public int typeObjective;
    public bool granaryEmpty;

    //Direction
    public float maxRangeWalk;

    public float identify;

    //Energie
    public float curEnergy;
    public float maxEnergie;
    public float minEnergie;

    //Life point
    public float curLife;
    public float maxLife;
    public float minLife;

    //Storage
    public float stockage;
    public float curStockage;

    //Position
    public float posx;
    public float posy;
    public float posz;
}

