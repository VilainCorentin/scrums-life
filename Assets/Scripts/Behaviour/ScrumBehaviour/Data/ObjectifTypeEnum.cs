﻿
public enum ObjectifTypeEnum
{
    NAN, //0
    FOOD, //1
    GRANARY, //2
    GRANARY_QUEEN, //3
    QUEEN, //4
    EGG, //5
    EGG_TRAVEL, //6
    EGG_FEED, //7
    COM, //8
    PUIT, //9
    DEAD, //10
    GO_ATTACK, //11
    IDLE_PLAYER, //12
}

