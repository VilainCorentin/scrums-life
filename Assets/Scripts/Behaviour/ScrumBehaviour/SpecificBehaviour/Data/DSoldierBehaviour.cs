﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class DSoldierBehaviour
{
    //Data parent
    public DScrumBehavior dataParent;

    //Spec for Soldier
    public float preyIdentify;
    public float LeaderIdentify;
    public List<float> listFollowerIdentify;

    public float pointToDefendx;
    public float pointToDefendy;
    public float pointToDefendz;

    public int maxFollower;
    public bool leaderIsPlayer;
    public bool isLeader;
}