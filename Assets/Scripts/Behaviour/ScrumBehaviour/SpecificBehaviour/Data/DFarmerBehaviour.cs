﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class DFarmerBehaviour
{
    //Data parent
    public DScrumBehavior dataParent;

    //Manage Puit
    public float puitPosx;
    public float puitPosy;
    public float puitPosz;

    public int nbDiffPuit;
    public int maxNbPuit;
}

