﻿using System;
using System.Collections.Generic;

[Serializable]
public class DNurseBehaviour
{
    //Data parent
    public DScrumBehavior dataParent;

    //My egg
    public DEgg eggData;

    public bool onCarring;
    public float minEnergyQueen;
    public float minEnergyReserve;
}