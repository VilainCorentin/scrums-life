﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NurseBehaviour : ScrumBehavior
{

    //Manage Nursery
    public GameObject myEgg;

    public DNurseBehaviour data;

    public AudioClip audioGrab;

    void Awake()
    {

        //Init dependencies
        this.manageSense = new ManageSenseNurse(this);
        this.manageObjectiveSpecific = new ManageObjectiveNurse(this);
        this.manageStorage = new ManageStorageNurse(this);

        //General init
        this.BasicStart();

        //Init Config
        GameObject.Find("Sun").GetComponent<ConfigNurse>().InitVar(this);

        //Init Data
        this.data = new DNurseBehaviour();
        this.data.eggData = null;
        this.data.dataParent = this.pData;

        //Init LifeGoal
        this.manageObjectiveSpecific.ResetObjective();

        //(Specific for Nurse)
        this.data.minEnergyQueen = 10f;
        this.data.onCarring = false;
        this.data.minEnergyReserve = 80;

    }

    void FixedUpdate()
    {

        this.manageUpdate.Update();

        this.RefreshPos();
    }

    //-------------------------------
    //SPECIFICS PUBLIC FUNCTION FOR NURSE
    //-------------------------------

    public void QueenFood()
    {

        if (CheckObjective((int)ObjectifTypeEnum.QUEEN))
        {

            float needFood = myQueen.GetComponent<QueenBehaviour>().data.maxEnergie - myQueen.GetComponent<QueenBehaviour>().data.curEnergy;

            if (this.pData.curStockage >= needFood)
            {
                myQueen.GetComponent<QueenBehaviour>().data.curEnergy += needFood;
                this.pData.curStockage -= needFood;
            }
            else
            {
                myQueen.GetComponent<QueenBehaviour>().data.curEnergy += this.pData.curStockage;
                this.pData.curStockage = 0;
            }

            myQueen.GetComponent<QueenBehaviour>().careLess = true;

        }

        this.pData.curEnergy--;

        Reroll();

    }

    public void EggFood()
    {

        //if objectif
        if (CheckObjective((int)ObjectifTypeEnum.EGG_FEED) && myEgg != null)
        {

            float needFood = myEgg.GetComponent<Egg>().data.energieToEvolve - myEgg.GetComponent<Egg>().data.curEnergy;

            bool evolve = false;

            if (needFood <= this.pData.curStockage)
            {
                evolve = true;
            }

            if (this.pData.curStockage >= needFood)
            {
                myEgg.GetComponent<Egg>().data.curEnergy += needFood;
                this.pData.curStockage -= needFood;
            }
            else
            {
                myEgg.GetComponent<Egg>().data.curEnergy += this.pData.curStockage;
                this.pData.curStockage = 0;
            }

            if (evolve)
            {
                myEgg = null;
                Reroll();
            }

            this.pData.curEnergy--;

        }
        else
        {
            Reroll();
        }


    }

    //-------------------------------
    //SPECIFICS PRIVATE FUNCTION FOR NURSE
    //-------------------------------

    private void Reroll()
    {
        this.manageObjective.ResetTimeObjective();
        this.manageMove.NewDirectionRandom();
    }

}
