﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class FarmerBehaviour : ScrumBehavior
{

    public DFarmerBehaviour data;

    public AudioClip audioEat;
    public AudioClip audioGive;
    public AudioClip audioCom;

    public Vector3 puitPos;

    void Awake()
    {
        //General init
        this.BasicStart();

        //Init Data
        this.data = new DFarmerBehaviour();
        this.data.dataParent = this.pData;

        //Init Config
        GameObject.Find("Sun").GetComponent<ConfigFarmer>().InitVar(this);

        //Init dependencies
        this.manageSense = new ManageSenseFarmer(this);
        this.manageCom = new ManageComFarmer(this);
        this.manageFoodSpecific = new ManageFoodFarmer(this);
        this.manageObjectiveSpecific = new ManageObjectiveFarmer(this);
        this.manageStorage = new ManageStorageFarmer(this);

        //Init LifeGoal
        this.manageObjectiveSpecific.ResetObjective();

        //(Specific for Farmer)
        this.data.nbDiffPuit = 0;
        this.data.maxNbPuit = 10;
        this.puitPos = new Vector3();

    }

    void FixedUpdate()
    {

        this.manageUpdate.Update();

        this.RefreshPos();
    }

    public void ReceiveStrategicPoint(Vector3 pos)
    {
        this.puitPos = pos;
        this.RefreshPosStr();

        if (this.data.nbDiffPuit <= 0)
        {
            this.data.nbDiffPuit = this.data.maxNbPuit;
        }
    }

    public void RefreshPosStr()
    {
        this.data.puitPosx = this.puitPos.x;
        this.data.puitPosy = this.puitPos.y;
        this.data.puitPosz = this.puitPos.z;
    }

}
