﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoldierBehaviour : ScrumBehavior
{

    public DSoldierBehaviour data;

    public Vector3 pointToDefend;

    //Manage Soldier
    public GameObject prey;
    public GameObject haveALeader;
    public List<GameObject> followers;

    public AudioClip audioHit;
    public AudioClip audioSpot;
    public AudioClip audioHitPlayer;

    void Awake()
    {
        //General init
        this.BasicStart();

        //Init Data
        this.data = new DSoldierBehaviour();
        this.data.preyIdentify = -1;
        this.data.LeaderIdentify = -1;
        this.data.listFollowerIdentify = new List<float>();
        this.data.dataParent = this.pData;

        //Init dependencies
        this.manageObjectiveSpecific = new ManageObjectiveSoldier(this);
        this.manageSense = new ManageSenseSoldier(this);
        this.manageStorage = new ManageStorageSoldier(this);
        this.manageFight = new ManageFightSoldier(this);

        //Init Config
        GameObject.Find("Sun").GetComponent<ConfigSoldier>().InitVar(this);

        this.data.maxFollower = 5;

        //Init LifeGoal
        this.manageObjectiveSpecific.ResetObjective();

        //(Specific for Soldier)
        this.haveALeader = null;
        this.pointToDefend = Vector3.zero;
        this.data.isLeader = false;
        this.data.leaderIsPlayer = false;

        float randomLeader = Random.Range(0f, 10f);
        if (randomLeader <= 1f)
        {
            Debug.Log("SOLDIER : Je suis un leader");
            this.data.isLeader = true;
            this.followers = new List<GameObject>();
        }

    }

    void FixedUpdate()
    {
        if (this != null)
        {
            this.manageUpdate.Update();

            this.RefreshPos();
        }
    }

    public bool IsAliveTarget(GameObject obj)
    {
        if (obj.GetComponent<ScrumBehavior>() != null)
        {
            if (obj.GetComponent<ScrumBehavior>().pData.curLife > 0 && obj.GetComponent<ScrumBehavior>().pData.curEnergy > 0)
            {
                return true;
            }
        }
        else if (obj.GetComponent<PlayerSpec>() != null)
        {
            if (obj.GetComponent<PlayerSpec>().data.curLife > 0)
            {
                return true;
            }
        }
        

        return false;
    }

    public void RefreshPosPointToDefend()
    {
        this.data.pointToDefendx = this.pointToDefend.x;
        this.data.pointToDefendy = this.pointToDefend.y;
        this.data.pointToDefendz = this.pointToDefend.z;
    }

}
