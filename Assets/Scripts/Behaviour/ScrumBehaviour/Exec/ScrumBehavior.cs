﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public abstract class ScrumBehavior : MonoBehaviour
{
    //Interface dependencies General
    public IMoveScrum manageMove;
    public IFood manageFood;
    public IObjectif manageObjective;
    public IStorage manageStorage;
    public IUpdate manageUpdate;

    //Interface dependencies Specifique
    public ISense manageSense;
    public ICommuncation manageCom;
    public IFoodSpecific manageFoodSpecific;
    public IObjectiveSpecific manageObjectiveSpecific;
    public IFight manageFight;

    //Data
    public DScrumBehavior pData;

    //Primals objectives
    public GameObject myGranary;
    public GameObject myQueen;
    public GameObject myNursery;

    //Agent for move
    public NavMeshAgent agent;
    public Vector3 previousBlockedPos;

    //Manage Time Objective
    public float nextAbandonObjectif;
    public float abandonObjectif;
    public float abandonTimeObjectivMin;
    public float abandonTimeObjectivMax;

    //Deboggage
    public float timeBlocked;
    public float nextTimeBlocked;
    public bool isDirectionBloqued;

    public AudioClip audioDie;
    public AudioClip audioOutPut;

    public void BasicStart()
    {

        this.audioDie = Resources.Load<AudioClip>("Sound/Noise/Die");
        this.audioOutPut = Resources.Load<AudioClip>("Sound/Noise/OutPutFood");

        //Init Data
        this.pData = new DScrumBehavior();

        //Init Move
        agent = GetComponent<NavMeshAgent>();
        GetComponent<NavMeshAgent>().nextPosition = transform.position;
        this.previousBlockedPos = transform.position;
        this.isDirectionBloqued = false;

        //Init dependencies
        this.manageFood = new ManageFoodGeneric(this);
        this.manageMove = new ManageMoveGeneric(this);
        this.manageObjective = new ManageObjectiveGeneric(this);
        this.manageUpdate = new ManageUpdateGeneric(this);

        //Identify
        this.pData.identify = GameObject.Find("Sun").GetComponent<InitGame>().GetIdentify();
    }

    public bool CheckObjective(int typeObj)
    {
        if (this.pData.typeObjective == typeObj)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool checkPath()
    {
        return !(this.agent.hasPath || this.agent.pathPending);
    }

    public void GetKill()
    {
        this.pData.curEnergy = -1;
    }

    public float Feed(float value)
    {
        float valueReturn = Mathf.Clamp(value, 0, this.pData.maxEnergie - this.pData.curEnergy);
        this.pData.curEnergy = this.pData.curEnergy + valueReturn;
        return valueReturn;
    }

    public void RefreshPos()
    {
        this.pData.posx = this.transform.position.x;
        this.pData.posy = this.transform.position.y;
        this.pData.posz = this.transform.position.z;
    }

    public bool CheckOwnQueen(GameObject obj)
    {
        if (this.myQueen != null && obj.GetComponent<ScrumBehavior>().myQueen != null)
        {
            float hisNumber = obj.GetComponent<ScrumBehavior>().myQueen.GetComponent<QueenBehaviour>().data.numberID;
            float myNumber = this.myQueen.GetComponent<QueenBehaviour>().data.numberID;

            if (hisNumber != myNumber)
            {
                return false;
            }
        }

        return true;
    }

}
