﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConfigNurse : MonoBehaviour
{
    public void InitVar(ScrumBehavior initScrum)
    {

        //Time Manager
        initScrum.nextTimeBlocked = 3f;
        initScrum.timeBlocked = initScrum.nextTimeBlocked + Time.time;
        initScrum.abandonTimeObjectivMin = 10;
        initScrum.abandonTimeObjectivMax = 30;
        initScrum.nextAbandonObjectif = Random.Range(initScrum.abandonTimeObjectivMin, initScrum.abandonTimeObjectivMax);
        initScrum.abandonObjectif = Time.time + initScrum.nextAbandonObjectif;

        //Max range for one move
        initScrum.pData.maxRangeWalk = 7f;

        //Init energy
        initScrum.pData.maxEnergie = 100f;
        initScrum.pData.curEnergy = initScrum.pData.maxEnergie;
        initScrum.pData.curStockage = 500f;
        initScrum.pData.stockage = 200f;
        initScrum.pData.minEnergie = 10f;

        //Init life
        initScrum.pData.maxLife = 50f;
        initScrum.pData.curLife = initScrum.pData.maxLife;
        initScrum.pData.minLife = 10f;

        //Objectif
        initScrum.pData.granaryEmpty = false;
        initScrum.myGranary = null;
        initScrum.myQueen = null;
        initScrum.myNursery = null;
    }

}
