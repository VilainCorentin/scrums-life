﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface RessourceQueenInterface
{

    void ResetFood();
    void SetFood(float food);

    void AddNurse(DNurseBehaviour scrum);
    void RemoveNurse(DNurseBehaviour scrum);

    void AddFarmer(DFarmerBehaviour scrum);
    void RemoveFarmer(DFarmerBehaviour scrum);

    void AddSoldier(DSoldierBehaviour scrum);
    void RemoveSoldier(DSoldierBehaviour scrum);

    void InitRessource();

    float getTotalFood();
    float getTotalSoldier();
    float getTotalEgg();
    float getTotalFarmer();
    float getTotalNurse();
    float getTotalPop();

    void setTotalFood(float val);
    void setTotalSoldier(float val);
    void setTotalEgg(float val);
    void setTotalFarmer(float val);
    void setTotalNurse(float val);
    void setTotalPop(float val);

    List<DFarmerBehaviour> getLstFarmer();
    List<DSoldierBehaviour> getLstSoldier();
    List<DNurseBehaviour> getLstNurse();

}
