﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class RessourceQueen : RessourceQueenInterface
{

    public float totalFood;

    public float totalPop;

    public float totalNurse;

    public float totalFarmer;

    public float totalEgg;

    public float totalSoldier;

    public List<DFarmerBehaviour> lst_Farmer;
    public List<DNurseBehaviour> lst_Nurse;
    public List<DSoldierBehaviour> lst_Soldier;

    public void SetFood(float food)
    {
        this.totalFood = food;
    }

    public void ResetFood()
    {
        this.totalFood = 0;
    }

    public void InitRessource()
    {
        this.lst_Farmer = new List<DFarmerBehaviour> { };
        this.lst_Nurse = new List<DNurseBehaviour> { };
        this.lst_Soldier = new List<DSoldierBehaviour> { };

        this.totalEgg = 0;
        this.totalFarmer = 0;
        this.totalFood = 0;
        this.totalNurse = 0;
        this.totalPop = 0;
        this.totalSoldier = 0;
    }

    public void AddNurse(DNurseBehaviour scrum)
    {
        this.lst_Nurse.Add(scrum);
        this.totalNurse++;
        this.totalPop++;
    }

    public void RemoveNurse(DNurseBehaviour scrum)
    {
        this.lst_Nurse.Remove(scrum);
        this.totalNurse--;
        this.totalPop--;
    }

    public void AddFarmer(DFarmerBehaviour scrum)
    {
        this.lst_Farmer.Add(scrum);
        this.totalFarmer++;
        this.totalPop++;
    }

    public void RemoveFarmer(DFarmerBehaviour scrum)
    {
        this.lst_Farmer.Remove(scrum);
        this.totalFarmer--;
        this.totalPop--;
    }

    public void AddSoldier(DSoldierBehaviour scrum)
    {
        this.lst_Soldier.Add(scrum);
        this.totalSoldier++;
        this.totalPop++;
    }

    public void RemoveSoldier(DSoldierBehaviour scrum)
    {
        this.lst_Soldier.Remove(scrum);
        this.totalSoldier--;
        this.totalPop--;
    }

    public float getTotalFood()
    {
        return this.totalFood;
    }
    public float getTotalSoldier()
    {
        return this.totalSoldier;
    }
    public float getTotalEgg()
    {
        return this.totalEgg;
    }
    public float getTotalFarmer()
    {
        return this.totalFarmer;
    }
    public float getTotalNurse()
    {
        return this.totalNurse;
    }
    public float getTotalPop()
    {
        return this.totalPop;
    }

    public void setTotalFood(float val)
    {
        this.totalFood = val;
    }
    public void setTotalSoldier(float val)
    {
        this.totalSoldier = val;
    }
    public void setTotalEgg(float val)
    {
        this.totalEgg = val;
    }
    public void setTotalFarmer(float val)
    {
        this.totalFarmer = val;
    }
    public void setTotalNurse(float val)
    {
        this.totalNurse = val;
    }
    public void setTotalPop(float val)
    {
        this.totalPop = val;
    }

    public List<DFarmerBehaviour> getLstFarmer()
    {
        return this.lst_Farmer;
    }
    public List<DSoldierBehaviour> getLstSoldier()
    {
        return this.lst_Soldier;
    }
    public List<DNurseBehaviour> getLstNurse()
    {
        return this.lst_Nurse;
    }

}
