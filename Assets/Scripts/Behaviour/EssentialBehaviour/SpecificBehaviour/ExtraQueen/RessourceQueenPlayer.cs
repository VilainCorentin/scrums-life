﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class RessourceQueenPlayer : RessourceQueen, RessourceQueenInterface
{

    public new void SetFood(float food)
    {
        base.SetFood(food);
        GameObject.Find("Sun").GetComponent<ManageUI>().SetFood(this.totalFood);
    }

    public new void ResetFood()
    {
        base.ResetFood();
        GameObject.Find("Sun").GetComponent<ManageUI>().SetFood(0f);
    }

    public new void InitRessource()
    {
        base.InitRessource();
    }

    public new void AddNurse(DNurseBehaviour scrum)
    {
        base.AddNurse(scrum);
        GameObject.Find("Sun").GetComponent<ManageUI>().IncreasePopulation();
    }

    public new void RemoveNurse(DNurseBehaviour scrum)
    {
        base.RemoveNurse(scrum);
        GameObject.Find("Sun").GetComponent<ManageUI>().DecreasePopulation();
    }

    public new void AddFarmer(DFarmerBehaviour scrum)
    {
        base.AddFarmer(scrum);
        GameObject.Find("Sun").GetComponent<ManageUI>().IncreasePopulation();
    }

    public new void RemoveFarmer(DFarmerBehaviour scrum)
    {
        base.RemoveFarmer(scrum);
        GameObject.Find("Sun").GetComponent<ManageUI>().DecreasePopulation();
    }

    public new void AddSoldier(DSoldierBehaviour scrum)
    {
        base.AddSoldier(scrum);
        GameObject.Find("Sun").GetComponent<ManageUI>().IncreasePopulation();
    }

    public new void RemoveSoldier(DSoldierBehaviour scrum)
    {
        base.RemoveSoldier(scrum);
        GameObject.Find("Sun").GetComponent<ManageUI>().DecreasePopulation();
    }

}
