﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class DQueenBehavior
{
    //Data parent
    public DEssentialBehavior dataParent;

    //Ressource
    public RessourceQueenInterface ressourceSave;

    //Data Dependencies
    public EggConfig eggConfig;
    public DGranarySoul granaryData;
    public DNurserySoul nurseData;

    //Spec Life
    public float curEnergy;
    public float maxEnergie;
    public float numberID;
}

