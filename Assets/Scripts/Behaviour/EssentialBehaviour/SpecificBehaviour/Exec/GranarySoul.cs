﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class GranarySoul : EssentialBehaviour
{

    public DGranarySoul data;

    public GameObject queen;

    void Start()
    {

        //Init Attribut
        this.BasicStart();

        //Init data
        this.data = new DGranarySoul();
        this.data.dataParent = this.pData;

        //Manage Food
        this.data.howManyFood = 500;
        this.data.maxFood = 10000;

        this.queen.GetComponent<QueenBehaviour>().data.granaryData = this.data;
    }

    void FixedUpdate()
    {
        this.Move();
    }

    public float EnterFood(float nbr)
    {
        float tmpFood = Mathf.Clamp(nbr, 0, this.data.maxFood - this.data.howManyFood);
        this.data.howManyFood = tmpFood + this.data.howManyFood;
        this.ManageSize();
        this.queen.GetComponent<QueenBehaviour>().ressource.SetFood(this.data.howManyFood);
        this.ManageHome();

        return tmpFood;
    }

    public float OutFood(float nbr)
    {

        float tmpFood = Mathf.Clamp(nbr, 0, this.data.howManyFood);

        this.data.howManyFood = this.data.howManyFood - tmpFood;

        this.ManageSize();
        this.ManageHome();

        this.queen.GetComponent<QueenBehaviour>().ressource.SetFood(this.data.howManyFood);

        return tmpFood;
    }

    private void ManageSize()
    {
        float size = this.data.howManyFood / 1000;
        if (size < 0.1)
        {
            size = 0.1f;
        }
        else if (size > 3.2f)
        {
            size = 3.2f;
        }
        this.transform.localScale = new Vector3(size, size, size);
    }
}
