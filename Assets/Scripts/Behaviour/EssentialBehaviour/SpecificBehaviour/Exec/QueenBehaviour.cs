﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class QueenBehaviour : EssentialBehaviour
{

    //Dependencies
    private EggGenerator generator;
    public RessourceQueenInterface ressource;

    //Data
    public DQueenBehavior data;

    //Spawner
    public GameObject spawner;

    // Queen Identify
    public static int NUMBERID = 0;

    //TODO: a delete apres
    public GameObject nurse;

    // My essentials
    public GameObject granarySoul;
    public GameObject nurserySoul;

    //Spec Spawn
    public float careLessTimer;
    public float spawnTimer;
    public float nextSpawnTimer;
    public float nextCareLessFree;
    public bool timerCareLessOn;

    //Init Manage of the queen by nurse
    public bool careLess;

    void Awake()
    {
        //Init Dependencies
        this.generator = GetComponent<EggGenerator>();
        this.data = new DQueenBehavior();
        this.data.eggConfig = this.generator.config;

        //Init Attribut
        this.BasicStart();

        //Init Data
        this.data.dataParent = this.pData;

        //Init energy
        this.data.maxEnergie = 100f;
        this.data.curEnergy = this.data.maxEnergie;
        //Init timer spawn
        this.nextSpawnTimer = 5f;
        this.nextCareLessFree = 30f;
        this.spawnTimer = this.nextSpawnTimer + Time.time;
        this.timerCareLessOn = false;

        // Init Identify
        this.data.numberID = NUMBERID;
        NUMBERID = NUMBERID + 1;

        GameObject.Find("Sun").GetComponent<InitGame>().ReadyForLoad();

        /*POUR LE DEV DU JEU SINON*/
        //SPAWN UNE NURSE POUR LE TEST
        Instantiate(nurse, new Vector3(spawner.transform.position.x, 1, spawner.transform.position.z), Quaternion.identity);
    }

    public void SpawnNursery(Vector3 pos)
    {
        this.nurserySoul = Instantiate(nurserySoul, new Vector3(pos.x, 1, pos.z), Quaternion.identity) as GameObject;
        this.nurserySoul.GetComponent<NurserySoul>().queen = this.gameObject;
        this.data.nurseData = this.nurserySoul.GetComponent<NurserySoul>().data;
    }

    public void SpawnGranary(Vector3 pos)
    {
        this.granarySoul = Instantiate(granarySoul, new Vector3(pos.x, 1, pos.z), Quaternion.identity) as GameObject;
        this.granarySoul.GetComponent<GranarySoul>().queen = this.gameObject;
        this.data.granaryData = this.granarySoul.GetComponent<GranarySoul>().data;
    }

    void FixedUpdate()
    {

        if (this.data.curEnergy > 0)
        {

            //For Spawn
            if (Time.time > this.spawnTimer)
            {

                this.ManageSpawn();

            }

            this.Move();

        }

        if (this.data.curEnergy < 10)
        {
            this.ManageCareByNurse();

        }
    }

    public void RefreshRessourceSave()
    {
        if (this.data.ressourceSave == null)
        {
            this.data.ressourceSave = this.ressource;
        }
    }

    private void ManageSpawn()
    {
        //Manage Speed by energy of the queen
        float parcentageTime = this.data.curEnergy / this.data.maxEnergie;
        float adaptativTimer = 0;

        if(parcentageTime < 1){
            adaptativTimer = this.nextSpawnTimer - (this.nextSpawnTimer * parcentageTime);
        }

        this.spawnTimer = this.nextSpawnTimer + adaptativTimer + Time.time;

        //Generate egg or not
        this.generator.GiveEgg(this);
    }

    //Manage if Queen is in Careless mode but no one care about or not and all is good
    //TODO: C'est possible qu'il faut modifer quelque condition 
    //car la reine va reset son careLess et donc une nurse peut venir essayer d'aider la reine alors qu'une autre nurse est déja en faite déja en train de l'aider... à voir si la nurse qui n'aura rien à faire va bien se reset et pas buger...
    private void ManageCareByNurse()
    {
        if (!this.timerCareLessOn)
        {
            this.careLessTimer = this.nextCareLessFree + Time.time;
            this.timerCareLessOn = true;
        }
        else
        {

            if (Time.time > this.careLessTimer)
            {
                this.careLess = true;
                this.timerCareLessOn = false;
            }

        }
    }

    public float Feed(float value)
    {
        float valueReturn = Mathf.Clamp(value, 0, this.data.maxEnergie - this.data.curEnergy);
        this.data.curEnergy = this.data.curEnergy + valueReturn;
        return valueReturn;
    }

}
