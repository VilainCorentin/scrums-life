﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NurserySoul : EssentialBehaviour
{

    public GameObject queen;

    public DNurserySoul data;

    void Start()
    {
        //Init Attribut
        this.BasicStart();

        //Init data
        this.data = new DNurserySoul();
        this.data.dataParent = this.pData;

        this.queen.GetComponent<QueenBehaviour>().data.nurseData = this.data;
    }

    void FixedUpdate()
    {
        this.Move();
    }

}
