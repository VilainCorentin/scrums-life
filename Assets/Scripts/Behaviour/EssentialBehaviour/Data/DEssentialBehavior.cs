﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class DEssentialBehavior
{
    //Manage come back to home
    public int goHome;

    public float posHomex;
    public float posHomey;
    public float posHomez;

    public int maxGoHome;

    //Manage move
    public float maxRangeWalk;

    //Follower
    public bool haveFollower;

}

