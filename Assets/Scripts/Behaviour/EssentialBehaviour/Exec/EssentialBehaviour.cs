﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EssentialBehaviour : MonoBehaviour
{
    
    // Manage Move
    public NavMeshAgent agent;

    //Follow player
    public GameObject follower;

    public Vector3 home;

    //Data
    public DEssentialBehavior pData;

    //Manage timer
    public float timeMove;
    public float nextTimeMove;
    public float nextTimeMoveMin;
    public float nextTimeMoveMax;

    public void BasicStart()
    {
        this.pData = new DEssentialBehavior();
        this.pData.goHome = 0;
        this.pData.maxGoHome = 10;
        this.home = this.transform.position;
        this.pData.maxRangeWalk = 3f;
        this.nextTimeMove = Random.Range(this.nextTimeMoveMin, this.nextTimeMoveMax);
        this.timeMove = this.nextTimeMove + Time.time;
        this.nextTimeMoveMin = 10;
        this.nextTimeMoveMax = 60;
        this.pData.haveFollower = false;

        this.RefreshHomePos();

        this.follower = null;

        this.agent = GetComponent<NavMeshAgent>();

    }

    protected void Move()
    {
        if (this.follower != null)
        {
            this.agent.SetDestination(follower.transform.position);
        }
        else if (Time.time > this.timeMove)
        {
            this.nextTimeMove = Random.Range(this.nextTimeMoveMin, this.nextTimeMoveMax);
            this.timeMove = Time.time + this.nextTimeMove;
            this.ManageHome();
        }

    }

    protected void ManageHome()
    {
        if (this.pData.goHome > this.pData.maxGoHome)
        {
            this.agent.SetDestination(this.home);
            this.pData.goHome = 0;
        }
        else
        {
            if (!this.agent.hasPath)
            {
                this.pData.goHome++;
                NewDirectionRandom();
            }
        }
    }

    private Vector3 NewDirectionRandom()
    {
        Vector3 randomDir = Random.insideUnitSphere * this.pData.maxRangeWalk;
        randomDir += this.transform.position;
        this.agent.SetDestination(randomDir);

        return randomDir;
    }

    public void RefreshHomePos()
    {
        this.pData.posHomex = this.home.x;
        this.pData.posHomey = this.home.y;
        this.pData.posHomez = this.home.z;
    }

}
