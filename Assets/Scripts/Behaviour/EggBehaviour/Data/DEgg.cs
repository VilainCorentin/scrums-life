﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class DEgg
{
    //Spec Life
    public float curEnergy;
    public float energieToEvolve;
    public float timeToLive;

    //State
    public bool carry;
    public bool takeCare;

    //What i am
    public int iAmThisIdentify;

    //Position
    public float posx;
    public float posy;
    public float posz;
}

