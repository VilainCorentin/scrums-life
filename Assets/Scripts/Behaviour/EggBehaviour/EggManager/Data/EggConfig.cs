﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class EggConfig
{

    private DEggConfig dec;

    public EggConfig()
    {
        dec = new DEggConfig();

        //Init array
        dec.sizeOfTypeScrum = System.Enum.GetNames(typeof(EggTypeEnum)).Length;
        dec.costForEvolve = new float[dec.sizeOfTypeScrum];
        dec.costForEgg = new float[dec.sizeOfTypeScrum];
        dec.delayForEgg = new float[dec.sizeOfTypeScrum];

        //Init dictionary
        dec.scrumProba = new Dictionary<int, int>();

        //Fill Array Cost
        dec.costForEvolve[(int)EggTypeEnum.FARMER] = dec.COST_FOR_FARMER;
        dec.costForEvolve[(int)EggTypeEnum.NURSE] = dec.COST_FOR_NURSE;
        dec.costForEvolve[(int)EggTypeEnum.SOLDIER] = dec.COST_FOR_SOLDIER;

        //Fill Array Cost Egg
        dec.costForEgg[(int)EggTypeEnum.FARMER] = dec.COST_FOR_FARMER_EGG;
        dec.costForEgg[(int)EggTypeEnum.NURSE] = dec.COST_FOR_NURSE_EGG;
        dec.costForEgg[(int)EggTypeEnum.SOLDIER] = dec.COST_FOR_SOLDIER_EGG;
        //Fill Array Delay Egg
        dec.delayForEgg[(int)EggTypeEnum.FARMER] = dec.DELAY_FOR_FARMER_EGG;
        dec.delayForEgg[(int)EggTypeEnum.NURSE] = dec.DELAY_FOR_NURSE_EGG;
        dec.delayForEgg[(int)EggTypeEnum.SOLDIER] = dec.DELAY_FOR_SOLDIER_EGG;

        //Fill dictionary for proba
        dec.scrumProba[(int)EggTypeEnum.FARMER] = dec.PROB_FOR_FARMER;
        dec.scrumProba[(int)EggTypeEnum.NURSE] = dec.PROB_FOR_NURSE;
        dec.scrumProba[(int)EggTypeEnum.SOLDIER] = dec.PROB_FOR_SOLDIER;

    }

    public float GetCost(int ind)
    {
        return dec.costForEvolve[ind];
    }

    public int GetSizeOfTypeScrum()
    {
        return dec.sizeOfTypeScrum;
    }

    public IDictionary<int, int> GetScrumProba()
    {
        return dec.scrumProba;
    }

    public float GetCostEgg(int ind)
    {
        return dec.costForEgg[ind];
    }

    public float GetTimeLife(int ind)
    {
        return dec.delayForEgg[ind];
    }

    public int GetBoostBegin()
    {
        return dec.BOOSTBEG;
    }

    public int GetBoostEnd()
    {
        return dec.BOOSTEND;
    }

    public float GetMaxProba()
    {
        return dec.MAXPROBA;
    }

    public float GetFailproba()
    {
        return dec.FAILEGG;
    }

}
