﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class DEggConfig
{
    
    //Dictionary for Proba
    public IDictionary<int, int> scrumProba;

    //Const for cost evolution
    public float COST_FOR_FARMER;
    public float COST_FOR_NURSE;
    public float COST_FOR_SOLDIER;

    //Const for cost egg
    public float COST_FOR_FARMER_EGG;
    public float COST_FOR_NURSE_EGG;
    public float COST_FOR_SOLDIER_EGG;

    //Const for time life of egg in seconds
    public float DELAY_FOR_FARMER_EGG;
    public float DELAY_FOR_NURSE_EGG;
    public float DELAY_FOR_SOLDIER_EGG;

    //Const for spawn proba
    public int PROB_FOR_FARMER;
    public int PROB_FOR_NURSE;
    public int PROB_FOR_SOLDIER;

    //Const for proba boost 
    public int BOOSTBEG;
    public int BOOSTEND;
    public float MAXPROBA;

    //In percentage for fail an egg
    public float FAILEGGPROBA;

    //The calcul
    public float FAILEGG;

    //Size of TypeScrum
    public int sizeOfTypeScrum;

    //Array link by TypeScrum
    public float[] costForEvolve;
    public float[] costForEgg;
    public float[] delayForEgg;

    public DEggConfig()
    {

        //Const for cost evolution
        COST_FOR_FARMER = 80;
        COST_FOR_NURSE = 200;
        COST_FOR_SOLDIER = 300;

        //Const for cost egg
        COST_FOR_FARMER_EGG = 2;
        COST_FOR_NURSE_EGG = 5;
        COST_FOR_SOLDIER_EGG = 5;

        //Const for time life of egg in seconds
        DELAY_FOR_FARMER_EGG = 600;
        DELAY_FOR_NURSE_EGG = 1000;
        DELAY_FOR_SOLDIER_EGG = 1000;

        //Const for spawn proba
        PROB_FOR_FARMER = 70;
        PROB_FOR_NURSE = 10;
        PROB_FOR_SOLDIER = 20;

        //Const for proba boost 
        BOOSTBEG = 0;
        BOOSTEND = 25;
        MAXPROBA = 100;

        //In percentage for fail an egg
        FAILEGGPROBA = 10;

        FAILEGG = (FAILEGGPROBA * MAXPROBA) / 100;
    }
}

