﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EggGenerator : MonoBehaviour
{

    public EggConfig config;
    public GameObject eggPrefab;

    //Type of scrum
    public GameObject[] typeOfScrum;
    public Color[] colorOfEgg;

    //Const for Color
    public Color COLOR_FOR_FARMER;
    public Color COLOR_FOR_NURSE;
    public Color COLOR_FOR_SOLDIER;

    private float energyToEvolveTmp;
    private float delayTimeTmp;
    private Color colorTmp;
    private GameObject iAmThisTmp;
    private int iAmThisIdentifyTmp;

    void Awake()
    {
        config = new EggConfig();

        this.typeOfScrum = new GameObject[3];
        this.typeOfScrum[0] = Resources.Load<GameObject>("Prefabs/LivingBeing/Scrum/Farmer");
        this.typeOfScrum[1] = Resources.Load<GameObject>("Prefabs/LivingBeing/Scrum/Nurse");
        this.typeOfScrum[2] = Resources.Load<GameObject>("Prefabs/LivingBeing/Scrum/Soldier");

        //Const for Color
        COLOR_FOR_FARMER = Color.green;
        COLOR_FOR_NURSE = Color.blue;
        COLOR_FOR_SOLDIER = Color.gray;

        colorOfEgg = new Color[this.typeOfScrum.Length];

        //Fill Array Color
        this.colorOfEgg[(int)EggTypeEnum.FARMER] = this.COLOR_FOR_FARMER;
        this.colorOfEgg[(int)EggTypeEnum.NURSE] = this.COLOR_FOR_NURSE;
        this.colorOfEgg[(int)EggTypeEnum.SOLDIER] = this.COLOR_FOR_SOLDIER;
    }

    public void GiveEgg(QueenBehaviour queen, int prob = -1)
    {

        //Generate type and attribut
        float minus = this.GenerateEgg(prob);

        if (minus != -1)
        {
            if (queen.data.curEnergy >= minus)
            {
                queen.data.curEnergy -= minus;

                GameObject eggTmp = Instantiate(eggPrefab, queen.spawner.transform.position, Quaternion.identity);
                Egg egg = eggTmp.GetComponent<Egg>();

                egg.data.energieToEvolve = this.energyToEvolveTmp;
                egg.data.timeToLive = this.delayTimeTmp;
                egg.iAmThis = this.iAmThisTmp;
                egg.data.iAmThisIdentify = this.iAmThisIdentifyTmp;
                eggTmp.GetComponent<Renderer>().material.color = this.colorTmp;
            }
        }

    }

    public float GenerateEgg(int prob)
    {

        float costEgg = 0;

        //If no buff for probabilities
        if (prob == -1)
        {
            costEgg = this.LegitGenerete();
        }
        else
        {
            costEgg = this.BoostGenerete(prob);
        }

        return costEgg;

    }

    public float RandomGenerator()
    {
        return Random.Range(0f, config.GetMaxProba() + config.GetFailproba());
    }

    private float LegitGenerete()
    {
        float rateSpawn = this.RandomGenerator();

        foreach (EggState state in this.GetListProba())
        {
            if (rateSpawn >= state.min && rateSpawn <= state.max)
            {
                return this.InitEgg(state.indTypeScrum);
            }
        }

        return -1;
    }

    private float BoostGenerete(int typeOfScrumBoost)
    {
        float rateSpawn = this.RandomGenerator();
        //One chance for 4 to create the needed egg
        if (rateSpawn >= config.GetBoostBegin() && rateSpawn <= config.GetBoostEnd())
        {
            return this.InitEgg(typeOfScrumBoost);
        }
        else
        {
            return this.LegitGenerete();
        }

    }

    private float InitEgg(int index)
    {
        this.energyToEvolveTmp = this.config.GetCost(index);
        this.iAmThisTmp = this.typeOfScrum[index];
        this.iAmThisIdentifyTmp = index;
        this.colorTmp = this.colorOfEgg[index];
        this.delayTimeTmp = this.config.GetTimeLife(index);
        return this.config.GetCostEgg(index);
    }

    private EggState[] GetListProba()
    {
        EggState[] list = new EggState[config.GetSizeOfTypeScrum()];
        float max = 0, min = 0;
        float limit = config.GetMaxProba();
        int total = 0, i = 0;

        //Calcul of total
        foreach (int proba in config.GetScrumProba().Values)
        {
            total += proba;
        }

        //Calcul of proba by type of scrum
        foreach (KeyValuePair<int, int> entry in config.GetScrumProba())
        {
            max = max + (entry.Value * limit) / total;
            list[i] = new EggState(entry.Key, min, max);
            min = max + 1;
            i++;
        }

        return list;
    }

}


