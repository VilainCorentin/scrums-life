﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EggState
{

    public int indTypeScrum;
    public float min;
    public float max;

    public EggState(int indScrum, float mi, float ma)
    {
        this.indTypeScrum = indScrum;
        this.min = mi;
        this.max = ma;
    }

}
