﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Egg : MonoBehaviour
{

    public DEgg data;

    //What i am
    public GameObject iAmThis;

    public AudioClip audioPop;

    //Timer
    private float spawnTime;
    private float nextSpawnTime;

    void Awake()
    {
        //Init Data
        this.data = new DEgg();

        //Init timer
        this.nextSpawnTime = 5f;
        this.spawnTime = Time.time + nextSpawnTime;

        //Egg start empty energy
        this.data.curEnergy = 0;

        //Init attributs for the gestion of this egg
        this.data.carry = false;
        this.data.takeCare = false;

    }

    void FixedUpdate()
    {
        if(gameObject != null){

            if (Time.time > spawnTime)
            {
                this.spawnTime = Time.time + nextSpawnTime;
                this.RefreshPos();
            }

            //Manage lifeTime
            this.data.timeToLive -= Time.deltaTime;
            if (this.data.timeToLive < 0 && !this.data.carry)
            {
                DestroyImmediate(gameObject);
            }

            if (this.data.curEnergy >= this.data.energieToEvolve)
            {
                //Evolution
                this.GetComponent<AudioSource>().PlayOneShot(this.audioPop);
                Instantiate(iAmThis, transform.position, Quaternion.identity);
                DestroyImmediate(gameObject);
            }
        }
    }

    public void RefreshPos()
    {
        this.data.posx = this.transform.position.x;
        this.data.posy = this.transform.position.y;
        this.data.posz = this.transform.position.z;
    }

    public float Feed(float value)
    {
        float valueReturn = Mathf.Clamp(value, 0, this.data.energieToEvolve - this.data.curEnergy);
        this.data.curEnergy = this.data.curEnergy + valueReturn;
        return valueReturn;
    }

    private bool CheckCarryCare()
    {
        if (this.data.carry || this.data.takeCare)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
