﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISense
{
    void FindByCac(GameObject obj);

    void FindByView(GameObject obj);
}
