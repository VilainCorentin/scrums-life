﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IObjectiveSpecific
{
    void ManageObjective();

    void ResetObjective();
}
