﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IFoodSpecific
{
    void EatCorps(GameObject obj);

    void EatFood(GameObject obj);

    void StoreFood(GameObject obj);

    void CalculFoodConsum(GameObject obj);
}
