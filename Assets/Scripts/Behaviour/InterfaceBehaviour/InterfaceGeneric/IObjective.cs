﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IObjectif
{
    void ResetTimeObjective();

    void ResetTimeObjective(int max);

    void ResetForUnlock();
}
