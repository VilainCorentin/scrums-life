﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMoveScrum
{
    Vector3 NewDirectionRandom();

    void UnlockPos();

    void GoGranary();

    void SetDirection(GameObject obj);

    void SetDirection(GameObject obj, int maxTime);

    void SetDirection(Vector3 pos);

    void ManageMove();
}
