﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IFood
{
    void GranaryOutFood(GameObject obj);

    void GranaryInFood(GameObject obj);
}
