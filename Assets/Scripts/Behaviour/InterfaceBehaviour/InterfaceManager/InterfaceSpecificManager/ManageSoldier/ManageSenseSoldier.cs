﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManageSenseSoldier : ISense
{

    protected SoldierBehaviour scrum;

    public ManageSenseSoldier(SoldierBehaviour scrumTarget)
    {
        this.scrum = scrumTarget;
    }

    public void FindByCac(GameObject obj)
    {
        if (obj.gameObject.tag == "Granary")
        {
            if (this.scrum.CheckObjective((int)ObjectifTypeEnum.GRANARY))
            {
                this.scrum.manageStorage.CalculStorage(obj.gameObject);
                this.scrum.manageObjectiveSpecific.ResetObjective();
            }
        }
        //Check if it's the target
        else if (this.scrum.CheckObjective((int)ObjectifTypeEnum.GO_ATTACK))
        {
            if (obj.gameObject.GetInstanceID() == this.scrum.prey.GetInstanceID())
            {
                //Attack target
                this.scrum.manageFight.Attack();
            }
        }
    }

    public void FindByView(GameObject obj)
    {
        if (obj.tag == "Queen")
        {

            if (this.scrum.myQueen == null)
            {
                this.scrum.myQueen = obj;
                this.scrum.myGranary = this.scrum.myQueen.GetComponent<QueenBehaviour>().granarySoul;
                this.scrum.myQueen.GetComponent<QueenBehaviour>().ressource.AddSoldier(this.scrum.data);

                if (this.scrum.myQueen.GetComponent<QueenBehaviour>().data.numberID != 0)
                {
                    this.scrum.transform.Find("DotLightEnn").gameObject.SetActive(true);
                }
            }
        }

        else if (obj.tag == "Granary")
        {

            if (this.scrum.myGranary == null)
            {
                this.scrum.myGranary = obj;
            }
            //If he saw him, i came AND i'm hungry
            else if (this.scrum.CheckObjective((int)ObjectifTypeEnum.GRANARY))
            {
                this.scrum.manageMove.SetDirection(obj);
            }

        }

        else if (obj.tag == "Farmer" || obj.tag == "Nurse")
        {
            //Check if not same colony
            if (!this.scrum.CheckOwnQueen(obj.gameObject) && this.scrum.IsAliveTarget(obj.gameObject))
            {

                InitGoFight(obj);
            }
        }

        else if (obj.tag == "Soldier")
        {
            if (this.scrum.CheckOwnQueen(obj.gameObject) && !obj.GetComponent<SoldierBehaviour>().CheckObjective((int)ObjectifTypeEnum.DEAD))
            {
                if (this.scrum.haveALeader == null && !this.scrum.data.isLeader && obj.GetComponent<SoldierBehaviour>().data.isLeader && obj.GetComponent<SoldierBehaviour>().followers.Count < obj.GetComponent<SoldierBehaviour>().data.maxFollower)
                {
                    this.scrum.haveALeader = obj.gameObject;
                    obj.GetComponent<SoldierBehaviour>().followers.Add(this.scrum.gameObject);
                }
                else if(this.scrum.haveALeader != null && !this.scrum.data.leaderIsPlayer)
                {
                    //If i find his dead leader
                    if (this.scrum.haveALeader.GetComponent<ScrumBehavior>().pData.identify == obj.GetComponent<ScrumBehavior>().pData.identify && obj.GetComponent<SoldierBehaviour>().CheckObjective((int)ObjectifTypeEnum.DEAD))
                    {
                        this.scrum.haveALeader = null;
                        this.scrum.manageObjectiveSpecific.ResetObjective();
                    }
                }
            }
            else if (this.scrum.IsAliveTarget(obj.gameObject))
            {
                InitGoFight(obj);
            }
        }
        else if (obj.tag == "Player")
        {
            if (this.scrum.myQueen.GetComponent<QueenBehaviour>().data.numberID != 0)
            {
                InitGoFight(obj);
            }
        }
    }

    private void InitGoFight(GameObject obj)
    {

        this.scrum.GetComponent<AudioSource>().PlayOneShot(this.scrum.audioSpot);

        this.scrum.pData.typeObjective = (int)ObjectifTypeEnum.GO_ATTACK;
        this.scrum.agent.angularSpeed = 300;
        this.scrum.agent.acceleration = 15;
        this.scrum.agent.speed = 15;
        this.scrum.prey = obj.gameObject;
    }

}
