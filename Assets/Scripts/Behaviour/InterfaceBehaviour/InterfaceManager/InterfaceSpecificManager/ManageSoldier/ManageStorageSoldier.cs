﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManageStorageSoldier : IStorage
{

    protected SoldierBehaviour scrum;

    public ManageStorageSoldier(SoldierBehaviour scrumTarget)
    {
        this.scrum = scrumTarget;
    }

    public void CalculStorage(GameObject obj)
    {
        //Take Food
        this.scrum.manageFood.GranaryOutFood(obj);
    }
}
