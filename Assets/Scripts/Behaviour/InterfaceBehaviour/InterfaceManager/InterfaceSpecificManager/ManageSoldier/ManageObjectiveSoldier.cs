﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManageObjectiveSoldier : IObjectiveSpecific
{

    protected SoldierBehaviour scrum;

    public ManageObjectiveSoldier(SoldierBehaviour scrumTarget)
    {
        this.scrum = scrumTarget;
    }


    public void ManageObjective()
    {
        //need food
        if (this.CheckEnergy())
        {
            this.scrum.pData.typeObjective = (int)ObjectifTypeEnum.GRANARY;
            this.scrum.agent.stoppingDistance = 0;
            this.scrum.manageMove.GoGranary();

            if (this.scrum.data.isLeader)
            {
                for (int i = 0; i < this.scrum.followers.Count; ++i )
                {
                    if (this.scrum.followers[i] != null)
                    {
                        this.scrum.followers[i].GetComponent<SoldierBehaviour>().haveALeader = null;
                        this.scrum.followers[i].GetComponent<SoldierBehaviour>().manageObjectiveSpecific.ResetObjective();
                    }
                    else
                    {
                        this.scrum.followers.Remove(this.scrum.followers[i]);
                    }
                }

                this.scrum.followers.Clear();
            }

        }

        if (this.scrum.CheckObjective((int)ObjectifTypeEnum.NAN))
        {
            if (this.scrum.haveALeader != null && this.scrum.haveALeader.tag == "Player")
            {
                if (this.scrum.agent.stoppingDistance == 0)
                {
                    this.scrum.agent.stoppingDistance = Random.Range(8f, 15f);
                }

                this.scrum.manageMove.SetDirection(this.scrum.haveALeader);
            }

            if(this.scrum.checkPath())
            {
                //Follow the leader
                if (this.scrum.haveALeader != null)
                {
                    this.scrum.manageMove.SetDirection(this.scrum.haveALeader);
                }
                else if (this.scrum.pointToDefend != Vector3.zero)
                {
                    Vector3 randomDir = Random.insideUnitSphere * this.scrum.pData.maxRangeWalk;
                    randomDir += this.scrum.pointToDefend;
                    this.scrum.manageMove.SetDirection(randomDir);
                }
            }
        }
        else if (this.scrum.CheckObjective((int)ObjectifTypeEnum.GO_ATTACK))
        {
            if (this.scrum.prey != null)
            {
                this.scrum.agent.stoppingDistance = 0;
                this.scrum.agent.SetDestination(this.scrum.prey.transform.position);
            }
        }
    }

    public void ResetObjective()
    {
        if (!this.scrum.CheckObjective((int)ObjectifTypeEnum.DEAD))
        {
            this.scrum.agent.ResetPath();
            this.scrum.pData.typeObjective = (int)ObjectifTypeEnum.NAN;
            this.scrum.agent.angularSpeed = 130;
            this.scrum.agent.acceleration = 8;
            this.scrum.agent.speed = 10;
            //TODO : Il faut virer cette ligne si on veut qu'un soldier focus jusque la mort !!!! sinon il abandonne
            this.scrum.prey = null;
            this.scrum.manageObjective.ResetTimeObjective();
            this.scrum.pData.curEnergy--;
        }
        else
        {
            GameObject.Find("Player").GetComponent<PlayerBehavior>().DelFollowers(this.scrum.gameObject);
            this.scrum.GetComponent<Animator>().enabled = false;
            this.scrum.transform.Find("DotLightEnn").gameObject.SetActive(false);
            this.scrum.GetComponent<AudioSource>().PlayOneShot(this.scrum.audioDie);
        }
    }

    private bool CheckEnergy()
    {
        if (this.scrum.pData.curEnergy <= this.scrum.pData.minEnergie && !this.scrum.pData.granaryEmpty)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
