﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManageFightSoldier : IFight
{

    protected SoldierBehaviour scrum;

    //Spec for fight
    float damage = 40;
    float speedAttack = 2;

    //Timer
    float attackTimer;

    public ManageFightSoldier(SoldierBehaviour scrumTarget)
    {
        this.scrum = scrumTarget;
        this.attackTimer = Time.time + speedAttack;
    }

    public void Attack()
    {
        if (Time.time > this.attackTimer && this.ChekPrey())
        {
            this.AttackChoosenTarget();
            this.attackTimer = this.speedAttack + Time.time;
        }
    }

    private bool ChekPrey()
    {
        if (this.scrum.prey != null)
        {
            if (this.scrum.IsAliveTarget(this.scrum.prey))
            {
                return true;
            }
            else
            {
                this.scrum.manageObjectiveSpecific.ResetObjective();
            }
        }

        return false;
    }

    private void AttackChoosenTarget()
    {

        float live = 1;

        if (this.scrum.prey.GetComponent<FarmerBehaviour>() != null)
        {
            live = this.scrum.prey.GetComponent<FarmerBehaviour>().pData.curLife -= this.damage;
            Debug.Log("ATTACK CONTRE UN FARMER !!!!!!!");
        }
        else if (this.scrum.prey.GetComponent<NurseBehaviour>() != null)
        {
            live = this.scrum.prey.GetComponent<NurseBehaviour>().pData.curLife -= this.damage;
            Debug.Log("ATTACK CONTRE UN NURSE !!!!!!!");
        }
        else if (this.scrum.prey.GetComponent<SoldierBehaviour>() != null)
        {
            live = this.scrum.prey.GetComponent<SoldierBehaviour>().pData.curLife -= this.damage;
            Debug.Log("ATTACK CONTRE UN SOLDIER !!!!!!!");
        }
        else if (this.scrum.prey.GetComponent<PlayerBehavior>() != null)
        {
            this.scrum.GetComponent<AudioSource>().PlayOneShot(this.scrum.audioHitPlayer);
            live = this.scrum.prey.GetComponent<PlayerBehavior>().data.curLife -= this.damage;
            this.scrum.prey.GetComponent<PlayerBehavior>().HUD.UpdateTextLifePlayer(this.scrum.prey.GetComponent<PlayerBehavior>().data.curLife, this.scrum.prey.GetComponent<PlayerBehavior>().data.maxLife);
            if (live <= 0)
            {
                this.scrum.manageObjectiveSpecific.ResetObjective();
            }
            return;
        }

        if(live <= 0){
            this.scrum.manageObjectiveSpecific.ResetObjective();
        }

        this.scrum.GetComponent<AudioSource>().PlayOneShot(this.scrum.audioHit);

    }

}
