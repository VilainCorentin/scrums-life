﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManageObjectiveNurse : IObjectiveSpecific
{

    protected NurseBehaviour scrum;

    //Manager timer for objective
    private int TimeForFeedQueen = 30;
    private int TimeToGoToQueen = 60;
    private float timeCheckToGoToQueen;

    public ManageObjectiveNurse(NurseBehaviour scrumTarget)
    {
        this.scrum = scrumTarget;
        this.timeCheckToGoToQueen = this.TimeToGoToQueen + Time.time;
    }

    public void ManageObjective()
    {
        if(this.scrum.checkPath())
        {
            if (this.scrum.CheckObjective((int)ObjectifTypeEnum.NAN))
            {
                //Back to granary for lunch
                if (this.CheckEnergy())
                {

                    this.scrum.pData.typeObjective = (int)ObjectifTypeEnum.GRANARY;
                    this.scrum.manageMove.GoGranary();

                }
                else
                {
                    //Go check for egg
                    if (this.timeCheckToGoToQueen < Time.time)
                    {

                        this.timeCheckToGoToQueen = this.TimeToGoToQueen + Time.time;
                        this.scrum.manageMove.SetDirection(this.scrum.myQueen.transform.position);

                    }
                }

                //Take care of the queen
                if (this.scrum.myQueen.GetComponent<QueenBehaviour>().data.curEnergy < this.scrum.data.minEnergyQueen && this.scrum.myQueen.GetComponent<QueenBehaviour>().careLess)
                {

                    //I take care about my queen
                    this.scrum.myQueen.GetComponent<QueenBehaviour>().careLess = false;

                    //Check if nurse have enought food for the queen
                    float QueenEnergyTmp = this.scrum.myQueen.GetComponent<QueenBehaviour>().data.maxEnergie - this.scrum.myQueen.GetComponent<QueenBehaviour>().data.curEnergy;
                    if (this.scrum.pData.curStockage < (QueenEnergyTmp) && !this.scrum.pData.granaryEmpty)
                    {
                        this.scrum.pData.typeObjective = (int)ObjectifTypeEnum.GRANARY_QUEEN;
                        this.scrum.manageObjective.ResetTimeObjective(TimeForFeedQueen);
                        this.scrum.agent.SetDestination(this.scrum.myGranary.transform.position);
                        Debug.Log("NURSE : cherche de la nourriture pour la Reine");

                    }
                    else
                    {

                        this.scrum.pData.typeObjective = (int)ObjectifTypeEnum.QUEEN;

                    }

                }

            }
            //Go feed the queen
            else if (this.scrum.CheckObjective((int)ObjectifTypeEnum.QUEEN))
            {

                this.scrum.agent.SetDestination(this.scrum.myQueen.transform.position);
                Debug.Log("NURSE : Va nourrir la Reine");

                //Go lead egg to nursery
            }
            else if (this.scrum.CheckObjective((int)ObjectifTypeEnum.EGG_TRAVEL))
            {

                this.scrum.agent.SetDestination(this.scrum.myNursery.transform.position);

            }
            else if (this.scrum.CheckObjective((int)ObjectifTypeEnum.EGG_FEED))
            {

                if (this.scrum.myEgg != null)
                {
                    if (this.scrum.pData.curStockage < this.scrum.data.minEnergyReserve)
                    {
                        this.scrum.manageMove.GoGranary();
                    }
                    else
                    {
                        this.scrum.agent.SetDestination(this.scrum.myEgg.transform.position);
                    }
                }
                else
                {
                    this.scrum.manageObjectiveSpecific.ResetObjective();
                }

            }
        }
    }

    public void ResetObjective()
    {
        if (this.scrum.CheckObjective((int)ObjectifTypeEnum.QUEEN) || this.scrum.CheckObjective((int)ObjectifTypeEnum.GRANARY_QUEEN))
        {
            this.scrum.myQueen.GetComponent<QueenBehaviour>().careLess = true;
        }
        if (this.scrum.myEgg != null)
        {
            this.scrum.myEgg.GetComponent<Egg>().data.carry = false;
            this.scrum.myEgg.GetComponent<Egg>().data.takeCare = false;
            this.scrum.myEgg = null;
        }

        if (this.scrum.transform.Find("CarryZone").transform.Find("Egg(Clone)") != null)
        {
            this.scrum.transform.Find("CarryZone").transform.Find("Egg(Clone)").transform.parent = null;
        }

        if (!this.scrum.CheckObjective((int)ObjectifTypeEnum.DEAD))
        {
            this.scrum.data.onCarring = false;
            this.scrum.isDirectionBloqued = false;
            this.scrum.pData.typeObjective = (int)ObjectifTypeEnum.NAN;
            this.scrum.manageObjective.ResetTimeObjective();
            this.scrum.pData.curEnergy--;
        }
        else
        {
            this.scrum.GetComponent<Animator>().enabled = false;
            this.scrum.transform.Find("DotLightEnn").gameObject.SetActive(false);
            this.scrum.GetComponent<AudioSource>().PlayOneShot(this.scrum.audioDie);
        }

    }

    private bool CheckEnergy()
    {
        if (this.scrum.pData.curEnergy <= this.scrum.pData.minEnergie && !this.scrum.pData.granaryEmpty)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

}
