﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManageSenseNurse : ISense
{

    protected NurseBehaviour scrum;

    //Manage time for go
    private int TimeForEgg = 15;

    public ManageSenseNurse(NurseBehaviour scrumTarget)
    {
        this.scrum = scrumTarget;
    }

    public void FindByCac(GameObject obj)
    {

        //Manage Queen
        if (obj.tag == "Queen" && this.scrum.CheckObjective((int)ObjectifTypeEnum.QUEEN))
        {

            this.scrum.QueenFood();
            this.scrum.manageObjectiveSpecific.ResetObjective();

        }

                //Manage nursery
        else if (obj.gameObject.tag == "Nursery")
        {
            //If lead egg to nursery
            if (this.scrum.CheckObjective((int)ObjectifTypeEnum.EGG_TRAVEL) && this.scrum.myEgg != null)
            {
                this.scrum.data.onCarring = false;
                this.scrum.myEgg.GetComponent<Rigidbody>().isKinematic = false;
                this.scrum.myEgg.GetComponent<Egg>().data.carry = false;

                if (this.scrum.transform.Find("CarryZone").transform.Find("Egg(Clone)") != null)
                {
                    this.scrum.transform.Find("CarryZone").transform.Find("Egg(Clone)").transform.parent = null;
                }
                this.scrum.pData.typeObjective = (int)ObjectifTypeEnum.EGG_FEED;
                this.scrum.agent.ResetPath();
            }
            else
            {
                this.scrum.manageObjectiveSpecific.ResetObjective();
            }
        }

        //Manage stockage
        else if (obj.gameObject.tag == "Granary")
        {
            this.scrum.manageStorage.CalculStorage(obj.gameObject);
        }

        //Take care of egg
        else if (obj.gameObject.tag == "Egg")
        {

            //If no care of anyEgg
            if (this.scrum.CheckObjective((int)ObjectifTypeEnum.NAN))
            {

                //So it's first time
                if (obj.gameObject.GetComponent<Egg>().data.carry == false && !this.scrum.data.onCarring)
                {

                    this.scrum.GetComponent<AudioSource>().PlayOneShot(this.scrum.audioGrab);

                    obj.gameObject.GetComponent<Egg>().data.carry = true;
                    this.scrum.data.onCarring = true;
                    obj.gameObject.GetComponent<Rigidbody>().isKinematic = true;
                    obj.gameObject.transform.parent = this.scrum.transform.Find("CarryZone");

                    this.scrum.myEgg = obj.gameObject;
                    this.scrum.myEgg.GetComponent<Egg>().data.takeCare = true;

                    this.scrum.pData.typeObjective = (int)ObjectifTypeEnum.EGG_TRAVEL;

                }

            }
            //Feed Egg
            else if (this.scrum.CheckObjective((int)ObjectifTypeEnum.EGG_FEED))
            {
                this.scrum.EggFood();
            }

        }

        this.scrum.isDirectionBloqued = false;

    }

    public void FindByView(GameObject obj)
    {
        if (obj.tag == "Queen")
        {

            if (this.scrum.myQueen == null)
            {
                this.scrum.myQueen = obj;
                this.scrum.myGranary = this.scrum.myQueen.GetComponent<QueenBehaviour>().granarySoul;
                this.scrum.myNursery = this.scrum.myQueen.GetComponent<QueenBehaviour>().nurserySoul;
                this.scrum.myQueen.GetComponent<QueenBehaviour>().ressource.AddNurse(this.scrum.data);
                

                if (this.scrum.myQueen.GetComponent<QueenBehaviour>().data.numberID != 0)
                {
                    this.scrum.transform.Find("DotLightEnn").gameObject.SetActive(true);
                }
            }
            else if (this.scrum.CheckObjective((int)ObjectifTypeEnum.QUEEN))
            {
                this.scrum.manageMove.SetDirection(obj);
            }


        }
        else if (obj.tag == "Granary")
        {

            if (this.scrum.myGranary == null)
            {
                this.scrum.myGranary = obj;

            }
            //If he saw him, i came AND i'm hungry
            else if (this.scrum.CheckObjective((int)ObjectifTypeEnum.QUEEN) || this.scrum.CheckObjective((int)ObjectifTypeEnum.GRANARY_QUEEN))
            {
                this.scrum.manageMove.SetDirection(obj);
            }

        }
        else if (obj.tag == "Nurse")
        {

            if (obj.GetComponent<NurseBehaviour>().myQueen == null && this.scrum.myQueen != null)
            {
                obj.GetComponent<NurseBehaviour>().myQueen = this.scrum.myQueen;
                obj.GetComponent<NurseBehaviour>().myGranary = this.scrum.myQueen.GetComponent<QueenBehaviour>().granarySoul;
                obj.GetComponent<NurseBehaviour>().myNursery = this.scrum.myQueen.GetComponent<QueenBehaviour>().nurserySoul;
                obj.GetComponent<NurseBehaviour>().myQueen.GetComponent<QueenBehaviour>().ressource.AddNurse(obj.GetComponent<NurseBehaviour>().data);

                if (this.scrum.myQueen.GetComponent<QueenBehaviour>().data.numberID != 0)
                {
                    obj.transform.Find("DotLightEnn").gameObject.SetActive(true);
                }
            }

        }
        else if (obj.tag == "Farmer")
        {

            //If no mother = adoption
            if (obj.GetComponent<FarmerBehaviour>().myQueen == null)
            {
                obj.GetComponent<FarmerBehaviour>().myQueen = this.scrum.myQueen;
                obj.GetComponent<FarmerBehaviour>().myGranary = this.scrum.myQueen.GetComponent<QueenBehaviour>().granarySoul;
                obj.GetComponent<FarmerBehaviour>().myQueen.GetComponent<QueenBehaviour>().ressource.AddFarmer(obj.GetComponent<FarmerBehaviour>().data);

                if (this.scrum.myQueen.GetComponent<QueenBehaviour>().data.numberID != 0)
                {
                    obj.transform.Find("DotLightEnn").gameObject.SetActive(true);
                }
            }
        }
        else if (obj.tag == "Soldier")
        {

            //If no mother = adoption
            if (obj.GetComponent<SoldierBehaviour>().myQueen == null)
            {
                obj.GetComponent<SoldierBehaviour>().myQueen = this.scrum.myQueen;
                obj.GetComponent<SoldierBehaviour>().myGranary = this.scrum.myQueen.GetComponent<QueenBehaviour>().granarySoul;
                obj.GetComponent<SoldierBehaviour>().myQueen.GetComponent<QueenBehaviour>().ressource.AddSoldier(obj.GetComponent<SoldierBehaviour>().data);

                if (this.scrum.myQueen.GetComponent<QueenBehaviour>().data.numberID != 0)
                {
                    obj.transform.Find("DotLightEnn").gameObject.SetActive(true);
                }
            }
        }
        else if (obj.tag == "Egg")
        {
            if (this.scrum.CheckObjective((int)ObjectifTypeEnum.NAN) && !this.scrum.isDirectionBloqued)
            {
                this.scrum.isDirectionBloqued = true;
                this.scrum.manageMove.SetDirection(obj, TimeForEgg);
            }
            //For catch the egg quickly
            else if (this.scrum.myEgg != null)
            {
                if (obj.gameObject.GetInstanceID() == this.scrum.myEgg.GetInstanceID())
                {
                    this.scrum.manageMove.SetDirection(obj, TimeForEgg);
                }
            }

        }

    }
}
