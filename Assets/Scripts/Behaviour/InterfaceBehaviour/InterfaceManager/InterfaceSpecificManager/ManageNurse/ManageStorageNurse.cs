﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManageStorageNurse : IStorage
{

    protected NurseBehaviour scrum;

    public ManageStorageNurse(NurseBehaviour scrumTarget)
    {
        this.scrum = scrumTarget;
    }

    public void CalculStorage(GameObject obj)
    {

        //if objectif
        if (this.scrum.CheckObjective((int)ObjectifTypeEnum.GRANARY))
        {
            //Take Food
            this.scrum.manageFood.GranaryOutFood(obj);

            this.scrum.manageObjectiveSpecific.ResetObjective();

        }
        else if (this.scrum.pData.typeObjective != (int)ObjectifTypeEnum.NAN)
        {

            if (this.scrum.pData.curStockage < this.scrum.pData.stockage)
            {
                
                float tmpStockage = this.scrum.pData.stockage - this.scrum.pData.curStockage;
                float energyRecup = obj.transform.GetComponent<GranarySoul>().OutFood(tmpStockage);
                this.scrum.pData.curStockage += energyRecup;
                if (energyRecup <= this.scrum.pData.minEnergie)
                {
                    this.scrum.pData.granaryEmpty = true;
                    Debug.Log("GRANARY : VIDE");
                }
                Debug.Log("GRANARY : Retire Food pour la Reine");
            }

            this.scrum.manageFood.GranaryOutFood(obj);

            this.scrum.manageObjective.ResetTimeObjective();

            if (this.scrum.CheckObjective((int)ObjectifTypeEnum.GRANARY_QUEEN))
            {
                this.scrum.pData.typeObjective = (int)ObjectifTypeEnum.QUEEN;
            }

        }

        //A changer selon l'action
        this.scrum.manageMove.NewDirectionRandom();

    }
}
