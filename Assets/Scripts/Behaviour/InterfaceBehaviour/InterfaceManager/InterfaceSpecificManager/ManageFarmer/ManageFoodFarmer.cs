﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManageFoodFarmer : IFoodSpecific
{

    protected FarmerBehaviour scrum;

    public ManageFoodFarmer(FarmerBehaviour scrumTarget)
    {
        this.scrum = scrumTarget;
    }

    public void EatCorps(GameObject obj)
    {

        this.scrum.GetComponent<AudioSource>().PlayOneShot(this.scrum.audioEat);

        if (obj.tag == "Nurse")
        {
            obj.GetComponent<NurseBehaviour>().myQueen.GetComponent<QueenBehaviour>().ressource.RemoveNurse(obj.GetComponent<NurseBehaviour>().data);
            this.scrum.pData.curEnergy += 50;
            MonoBehaviour.Destroy(obj);
            Debug.Log("Cadavre mangé : Nurse");
        }
        else if (obj.tag == "Farmer")
        {
            if (obj.GetComponent<FarmerBehaviour>().pData.curStockage > 0)
            {
                this.scrum.pData.curEnergy = Mathf.Clamp(obj.GetComponent<FarmerBehaviour>().pData.curStockage + this.scrum.pData.curEnergy, 0, this.scrum.pData.maxEnergie);
            }
            else
            {
                this.scrum.pData.curEnergy += 50;
            }

            obj.GetComponent<FarmerBehaviour>().myQueen.GetComponent<QueenBehaviour>().ressource.RemoveFarmer(obj.GetComponent<FarmerBehaviour>().data);

            MonoBehaviour.Destroy(obj);
            Debug.Log("Cadavre mangé : Farmer");
        }
        else if (obj.tag == "Soldier")
        {
            obj.GetComponent<SoldierBehaviour>().myQueen.GetComponent<QueenBehaviour>().ressource.RemoveSoldier(obj.GetComponent<SoldierBehaviour>().data);
            this.scrum.pData.curEnergy += 100;
            MonoBehaviour.Destroy(obj);
            Debug.Log("Cadavre mangé : Soldier");
        }

    }

    public void EatFood(GameObject obj)
    {

        this.scrum.GetComponent<AudioSource>().PlayOneShot(this.scrum.audioEat);

        float tmpEnergy = this.scrum.pData.maxEnergie - this.scrum.pData.curEnergy;

        if (obj.GetComponent<Foodspec>().foodPower >= tmpEnergy)
        {

            this.scrum.pData.curEnergy += tmpEnergy;
            obj.GetComponent<Foodspec>().foodPower -= tmpEnergy;

        }
        else
        {

            this.scrum.pData.curEnergy += obj.GetComponent<Foodspec>().foodPower;
            obj.GetComponent<Foodspec>().foodPower = 0;

        }
    }

    public void StoreFood(GameObject obj)
    {

        if (obj.GetComponent<Foodspec>().foodPower > 0)
        {

            float tmpStockage = this.scrum.pData.stockage - this.scrum.pData.curStockage;

            if (obj.GetComponent<Foodspec>().foodPower >= tmpStockage)
            {

                this.scrum.pData.curStockage += tmpStockage;
                obj.GetComponent<Foodspec>().foodPower -= tmpStockage;

            }
            else
            {

                this.scrum.pData.curStockage += obj.GetComponent<Foodspec>().foodPower;
                obj.GetComponent<Foodspec>().foodPower = 0;

            }

            Debug.Log("SCRUM : Stockage plein");

        }
    }

    public void CalculFoodConsum(GameObject obj)
    {
        //EAT AND STOCK AFTER
        //Manage EAT -----------
        this.scrum.manageFoodSpecific.EatFood(obj);
        //-----------

        //Manage stockage
        this.scrum.manageFoodSpecific.StoreFood(obj);
        //------------

        this.scrum.pData.granaryEmpty = false;
    }


}
