﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManageComFarmer : ICommuncation
{

    protected FarmerBehaviour scrum;

    public ManageComFarmer(FarmerBehaviour scrumTarget)
    {
        this.scrum = scrumTarget;
    }

    public void CommunicationAllie(GameObject obj)
    {

        this.scrum.GetComponent<AudioSource>().PlayOneShot(this.scrum.audioCom);

        if (obj.tag == "Farmer")
        {
            //I want to spread my pos for puit
            if (this.scrum.data.nbDiffPuit > 0)
            {
                if (obj.GetComponent<FarmerBehaviour>().data.nbDiffPuit == 0)
                {
                    obj.GetComponent<FarmerBehaviour>().puitPos = this.scrum.puitPos;
                    obj.GetComponent<FarmerBehaviour>().RefreshPosStr();
                    obj.GetComponent<ScrumBehavior>().pData.typeObjective = (int)ObjectifTypeEnum.PUIT;

                    obj.GetComponent<FarmerBehaviour>().data.nbDiffPuit = obj.GetComponent<FarmerBehaviour>().data.maxNbPuit;
                    this.scrum.data.nbDiffPuit--;

                    this.scrum.pData.typeObjective = (int)ObjectifTypeEnum.NAN;

                    Debug.Log("ANT : Diffusion du Puit");
                }
            }
        }

        else if (obj.tag == "Soldier")
        {
            if (obj.GetComponent<SoldierBehaviour>().pointToDefend == Vector3.zero)
            {
                obj.GetComponent<SoldierBehaviour>().pointToDefend = this.scrum.puitPos;
                obj.GetComponent<SoldierBehaviour>().RefreshPosPointToDefend();
                obj.GetComponent<SoldierBehaviour>().manageObjectiveSpecific.ResetObjective();
            }
        }

    }

}
