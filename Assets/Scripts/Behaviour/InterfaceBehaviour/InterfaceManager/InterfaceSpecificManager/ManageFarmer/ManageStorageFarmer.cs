﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManageStorageFarmer : IStorage
{

    protected FarmerBehaviour scrum;

    public ManageStorageFarmer(FarmerBehaviour scrumTarget)
    {
        this.scrum = scrumTarget;
    }

    public void CalculStorage(GameObject obj)
    {

        //if objectif
        if (this.scrum.CheckObjective((int)ObjectifTypeEnum.GRANARY))
        {

            this.scrum.GetComponent<AudioSource>().PlayOneShot(this.scrum.audioGive);

            //Enter Foods
            this.scrum.manageFood.GranaryInFood(obj);

            //Take Food
            if (this.scrum.pData.curEnergy <= this.scrum.pData.minEnergie)
            {
                this.scrum.manageFood.GranaryOutFood(obj);
            }
        }
    }
}
