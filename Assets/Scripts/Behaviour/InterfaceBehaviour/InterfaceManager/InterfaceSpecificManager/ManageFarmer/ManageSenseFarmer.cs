﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManageSenseFarmer : ISense
{
    protected FarmerBehaviour scrum;

    //Manage Communication
    private float minEnergyForTalkPuit = 20;

    public ManageSenseFarmer(FarmerBehaviour scrumTarget)
    {
        this.scrum = scrumTarget;
    }

    public void FindByCac(GameObject obj)
    {

        if (obj != null)
        {

            //Contact with other Ant
            if (obj.tag == "Farmer")
            {
                //TODO: IL FAUT FAIRE LA VERIF SI ON EST ALLIE OU PAS...
                if (this.scrum.CheckObjective((int)ObjectifTypeEnum.COM))
                {
                    this.scrum.manageCom.CommunicationAllie(obj.gameObject);
                }


                //I eat dead body
                if (obj.GetComponent<FarmerBehaviour>().CheckObjective((int)ObjectifTypeEnum.DEAD))
                {
                    this.scrum.manageFoodSpecific.EatCorps(obj);

                }

                this.scrum.manageObjectiveSpecific.ResetObjective();

            }

            //clean up dead body
            else if (obj.gameObject.tag == "Nurse")
            {

                if (obj.GetComponent<NurseBehaviour>().CheckObjective((int)ObjectifTypeEnum.DEAD))
                {
                    this.scrum.manageFoodSpecific.EatCorps(obj);
                }

            }

            else if (obj.gameObject.tag == "Soldier")
            {

                if (obj.GetComponent<SoldierBehaviour>().CheckObjective((int)ObjectifTypeEnum.DEAD))
                {
                    this.scrum.manageFoodSpecific.EatCorps(obj);
                }
                else if (this.scrum.CheckObjective((int)ObjectifTypeEnum.COM) && obj.GetComponent<SoldierBehaviour>().CheckObjective((int)ObjectifTypeEnum.COM) && obj.GetComponent<SoldierBehaviour>().data.isLeader)
                {
                    this.scrum.manageCom.CommunicationAllie(obj);
                }

            }

            //Manage stockage
            else if (obj.gameObject.tag == "Granary")
            {
                if (this.scrum.CheckObjective((int)ObjectifTypeEnum.GRANARY))
                {
                    this.scrum.manageStorage.CalculStorage(obj.gameObject);
                    this.scrum.manageObjectiveSpecific.ResetObjective();
                }
            }

            //Manage food finding
            else if (obj.gameObject.tag == "Food")
            {

                this.scrum.manageFoodSpecific.CalculFoodConsum(obj.gameObject);
                this.scrum.manageObjectiveSpecific.ResetObjective();

            }
        }

    }

    public void FindByView(GameObject obj)
    {
        if (obj.tag == "Food")
        {

            //Move to objectif
            if (!this.scrum.CheckObjective((int)ObjectifTypeEnum.GRANARY))
            {
                this.scrum.pData.typeObjective = (int)ObjectifTypeEnum.FOOD;
                this.scrum.manageMove.SetDirection(obj);
            }

        }

        else if (obj.tag == "Queen")
        {

            if (this.scrum.myQueen == null)
            {
                this.scrum.myQueen = obj;
                this.scrum.myGranary = this.scrum.myQueen.GetComponent<QueenBehaviour>().granarySoul;
                this.scrum.myQueen.GetComponent<QueenBehaviour>().ressource.AddFarmer(this.scrum.data);

                if(this.scrum.myQueen.GetComponent<QueenBehaviour>().data.numberID != 0)
                {
                    this.scrum.transform.Find("DotLightEnn").gameObject.SetActive(true);
                }
            }

        }

        //Manage dead Body
        else if ((obj.tag == "Nurse" || obj.tag == "Soldier" || obj.tag == "Farmer") && obj.GetComponent<ScrumBehavior>().CheckObjective((int)ObjectifTypeEnum.DEAD))
        {
            this.scrum.manageMove.SetDirection(obj);
        }

        else if (obj.tag == "Granary")
        {

            if (this.scrum.myGranary == null)
            {
                this.scrum.myGranary = obj;

            }
            else if (this.scrum.CheckObjective((int)ObjectifTypeEnum.GRANARY))
            {
                //If he saw him, i came
                this.scrum.manageMove.SetDirection(obj);
            }

        }

        else if (obj.tag == "Puit")
        {

            //this.scrum.typeObjective = (int)ObjectifTypeEnum.PUIT;
            if (this.scrum.data.nbDiffPuit == 0 && !this.scrum.CheckObjective((int)ObjectifTypeEnum.PUIT))
            {
                this.scrum.data.nbDiffPuit = this.scrum.data.maxNbPuit;
                this.scrum.puitPos = obj.transform.position;
                this.scrum.RefreshPosStr();
            }

        }

        else if (obj.tag == "Farmer")
        {

            //If no mother = adoption
            if (obj.GetComponent<FarmerBehaviour>().myQueen == null)
            {
                obj.GetComponent<FarmerBehaviour>().myQueen = this.scrum.myQueen;
                obj.GetComponent<FarmerBehaviour>().myGranary = this.scrum.myQueen.GetComponent<QueenBehaviour>().granarySoul;
                obj.GetComponent<FarmerBehaviour>().myQueen.GetComponent<QueenBehaviour>().ressource.AddFarmer(this.scrum.data);

                if (this.scrum.myQueen.GetComponent<QueenBehaviour>().data.numberID != 0)
                {
                    this.scrum.transform.Find("DotLightEnn").gameObject.SetActive(true);
                }
            }

            //If they are of the same mother
            else
            {

                //TODO: Il FAUT METTRE UN NUMERO A LA REINE !!!
                //if (obj.GetComponent<Antbehaviour> ().myQueen.name == this.myQueen.name) {

                //Spread position of Puit
                if (this.scrum.CheckObjective((int)ObjectifTypeEnum.PUIT))
                {
                    if (this.CheckIfDispoForPuit(obj) && this.CheckDispoCom(obj))
                    {

                        //Tell him the pos of Food
                        if (!obj.GetComponent<FarmerBehaviour>().CheckObjective((int)ObjectifTypeEnum.PUIT)
                            && obj.GetComponent<FarmerBehaviour>().pData.curEnergy >= minEnergyForTalkPuit)
                        {
                            this.GoCommunication(obj);
                        }
                    }
                }
                //}
            }
        }

        else if (obj.tag == "Nurse")
        {

            if (obj.GetComponent<NurseBehaviour>().CheckObjective((int)ObjectifTypeEnum.DEAD))
            {
                this.scrum.manageMove.SetDirection(obj);
                Debug.Log("VUE : Une nurse sans vie");
            }

        }

            //Give a point to defend
        else if (obj.tag == "Soldier")
        {
            SoldierBehaviour soldier = obj.GetComponent<SoldierBehaviour>();
            if (this.scrum.CheckOwnQueen(obj) && soldier.data.isLeader && soldier.CheckObjective((int)ObjectifTypeEnum.NAN) && soldier.pointToDefend == Vector3.zero && this.scrum.data.nbDiffPuit > 0)
            {
                this.GoCommunication(obj);
            }
        }

    }

    private bool CheckIfDispoForPuit(GameObject obj)
    {
        if (obj.GetComponent<FarmerBehaviour>().data.nbDiffPuit == 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private bool CheckDispoCom(GameObject obj)
    {
        if (!this.scrum.CheckObjective((int)ObjectifTypeEnum.COM)
            && !obj.GetComponent<ScrumBehavior>().CheckObjective((int)ObjectifTypeEnum.COM))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private void GoCommunication(GameObject obj)
    {
        Vector3 fixedPostion = obj.GetComponent<ScrumBehavior>().transform.position;

        //Tell him the pos of Food
        obj.GetComponent<ScrumBehavior>().pData.typeObjective = (int)ObjectifTypeEnum.COM;
        this.scrum.pData.typeObjective = (int)ObjectifTypeEnum.COM;

        obj.GetComponent<ScrumBehavior>().manageMove.SetDirection(fixedPostion);
        this.scrum.manageMove.SetDirection(obj);
    }

}
