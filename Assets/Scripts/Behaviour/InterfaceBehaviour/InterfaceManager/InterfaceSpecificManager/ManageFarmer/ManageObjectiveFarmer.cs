﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManageObjectiveFarmer : IObjectiveSpecific
{

    protected FarmerBehaviour scrum;

    public ManageObjectiveFarmer(FarmerBehaviour scrumTarget)
    {
        this.scrum = scrumTarget;
    }

    public void ManageObjective()
    {
        if (this.scrum.checkPath())
        {
            if (this.scrum.CheckObjective((int)ObjectifTypeEnum.NAN))
            {
                if ((this.CheckStorage() || this.CheckEnergy()))
                {
                    this.scrum.pData.typeObjective = (int)ObjectifTypeEnum.GRANARY;
                    this.scrum.manageMove.GoGranary();
                }
                else if (this.scrum.data.nbDiffPuit > 0)
                {
                    this.scrum.pData.typeObjective = (int)ObjectifTypeEnum.PUIT;

                    //Go to the puit
                    this.scrum.manageMove.SetDirection(this.scrum.puitPos);

                    //Decrease memory of the pos Puit
                    this.scrum.data.nbDiffPuit--;

                    //Forget the puit if he need it
                    this.ForgetPuit();
                }
            }
            else if (this.scrum.CheckObjective((int)ObjectifTypeEnum.GRANARY))
            {
                this.scrum.manageMove.GoGranary();
            }
        }

    }

    public void ResetObjective()
    {
        if (!this.scrum.CheckObjective((int)ObjectifTypeEnum.DEAD))
        {
            this.scrum.agent.ResetPath();
            this.scrum.pData.typeObjective = (int)ObjectifTypeEnum.NAN;
            this.scrum.manageObjective.ResetTimeObjective();
            this.scrum.pData.curEnergy--;
        }
        else
        {
            this.scrum.GetComponent<Animator>().enabled = false;
            this.scrum.transform.Find("DotLightEnn").gameObject.SetActive(false);
            this.scrum.GetComponent<AudioSource>().PlayOneShot(this.scrum.audioDie);
        }
    }

    private bool CheckEnergy()
    {
        if (this.scrum.pData.curEnergy <= this.scrum.pData.minEnergie && !this.scrum.pData.granaryEmpty && this.scrum.checkPath())
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private bool CheckStorage()
    {
        if (this.scrum.pData.curStockage >= this.scrum.pData.stockage && this.scrum.checkPath())
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private void ForgetPuit()
    {
        if(this.scrum.data.nbDiffPuit == 0){
            this.scrum.manageObjectiveSpecific.ResetObjective();
        }
    }

}
