﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManageObjectiveGeneric : IObjectif
{

    protected ScrumBehavior scrum;

    public ManageObjectiveGeneric(ScrumBehavior scrumTarget)
    {
        this.scrum = scrumTarget;
    }

    public void ResetForUnlock()
    {
        this.scrum.manageObjectiveSpecific.ResetObjective();
        this.scrum.manageMove.NewDirectionRandom();
        this.ResetTimeObjective();
    }

    public void ResetTimeObjective()
    {
        this.scrum.nextAbandonObjectif = Random.Range(this.scrum.abandonTimeObjectivMin, this.scrum.abandonTimeObjectivMax);
        this.scrum.pData.granaryEmpty = false;
        this.scrum.abandonObjectif = Time.time + this.scrum.nextAbandonObjectif;
    }

    public void ResetTimeObjective(int max)
    {
        this.scrum.nextAbandonObjectif = max;
        this.scrum.pData.granaryEmpty = false;
        this.scrum.abandonObjectif = Time.time + this.scrum.nextAbandonObjectif;
    }
}
