﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManageFoodGeneric : IFood
{

    protected ScrumBehavior scrum;

    public ManageFoodGeneric(ScrumBehavior scrumTarget)
    {
        this.scrum = scrumTarget;
    }

    public void GranaryOutFood(GameObject obj)
    {
        float tmpEnergy = this.scrum.pData.maxEnergie - this.scrum.pData.curEnergy;
        float energyRecup = obj.transform.GetComponent<GranarySoul>().OutFood(tmpEnergy);
        this.scrum.pData.curEnergy += energyRecup;
        if (energyRecup <= this.scrum.pData.minEnergie)
        {
            this.scrum.pData.granaryEmpty = true;
            Debug.Log("GRANARY : VIDE");
        }
        Debug.Log("GRANARY : Retire Food");
    }

    public void GranaryInFood(GameObject obj)
    {
        obj.transform.GetComponent<GranarySoul>().EnterFood(this.scrum.pData.curStockage);
        this.scrum.pData.curStockage = 0;
        Debug.Log("GRANARY : Stockage Food");
    }

}
