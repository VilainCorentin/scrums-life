﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManageUpdateGeneric : IUpdate
{

    protected ScrumBehavior scrum;

    public ManageUpdateGeneric(ScrumBehavior scrumTarget)
    {
        this.scrum = scrumTarget;
    }

    public void Update()
    {
        if (this.scrum.pData.typeObjective != ((int)ObjectifTypeEnum.DEAD))
        {
            //Manage energy and life
            if (this.scrum.pData.curEnergy > 0 && this.scrum.pData.curLife > 0)
            {

                //Manage Moves-------------
                this.scrum.manageMove.ManageMove();

                //Manage Objectives-------------
                if (this.scrum.myQueen != null)
                {
                    this.scrum.manageObjectiveSpecific.ManageObjective();
                }

            }
            else
            {

                //Die
                this.scrum.pData.typeObjective = (int)ObjectifTypeEnum.DEAD;
                this.scrum.manageObjectiveSpecific.ResetObjective();
                this.scrum.GetComponent<Rigidbody>().isKinematic = false;
                this.scrum.agent.enabled = false;
            }
        }
    }
}
