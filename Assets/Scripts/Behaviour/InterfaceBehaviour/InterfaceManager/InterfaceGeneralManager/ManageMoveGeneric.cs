﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManageMoveGeneric : IMoveScrum
{

    protected ScrumBehavior scrum;

    public ManageMoveGeneric(ScrumBehavior scrumTarget)
    {
        this.scrum = scrumTarget;
    }

    public Vector3 NewDirectionRandom()
    {
        Vector3 randomDir = Random.insideUnitSphere * this.scrum.pData.maxRangeWalk;
        randomDir += this.scrum.transform.position;

        this.scrum.agent.ResetPath();
        this.scrum.manageMove.SetDirection(randomDir);

        this.scrum.pData.curEnergy--;

        return randomDir;
    }

    public void UnlockPos()
    {
        if (Time.time > this.scrum.timeBlocked)
        {

            this.scrum.timeBlocked = this.scrum.nextTimeBlocked + Time.time;

            Vector3 actualPos = new Vector3(Mathf.Round(this.scrum.transform.position.x), Mathf.Round(this.scrum.transform.position.y), Mathf.Round(this.scrum.transform.position.z));

            if (this.scrum.previousBlockedPos == actualPos)
            {
                this.scrum.manageMove.NewDirectionRandom();
                this.scrum.pData.curEnergy--;
                Debug.Log("SCRUM : Debloquage, Nouvelle direction");

            }
            else
            {
                this.scrum.previousBlockedPos = new Vector3(Mathf.Round(this.scrum.transform.position.x), Mathf.Round(this.scrum.transform.position.y), Mathf.Round(this.scrum.transform.position.z));
            }

            //TODO: Il faut le mettre dans le IF au dessus si on veut que les scrums perdent moins de vie
            //this.scrum.curEnergy--;

        }
    }

    public void GoGranary()
    {
        this.scrum.manageMove.SetDirection(this.scrum.myGranary, 30);
    }


    public void SetDirection(GameObject obj)
    {
        if (obj != null)
        {
            this.scrum.agent.SetDestination(obj.transform.position);
            this.scrum.manageObjective.ResetTimeObjective();
        }
    }

    public void SetDirection(Vector3 pos)
    {
        if (pos != Vector3.zero)
        {
            this.scrum.agent.SetDestination(pos);
            this.scrum.manageObjective.ResetTimeObjective();
        }
    }

    public void SetDirection(GameObject obj, int maxTime)
    {
        if (obj != null)
        {
            this.scrum.agent.SetDestination(obj.transform.position);
            this.scrum.manageObjective.ResetTimeObjective(maxTime);
        }
    }


    public void ManageMove()
    {
        //if no objectif
        if (this.scrum.CheckObjective((int)ObjectifTypeEnum.NAN))
        {

            //If nolock and no path
            if (this.scrum.checkPath())
            {
                this.scrum.manageMove.NewDirectionRandom();
                Debug.Log("SCRUM : Balade");

            }

        }
        else
        {

            //With objectif
            if (Time.time > this.scrum.abandonObjectif)
            {

                this.scrum.manageObjective.ResetForUnlock();

            }
        }

        //Unlock if blocked
        this.scrum.manageMove.UnlockPos();
    }
}
