﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class PlayerSave
{
    private float curLife;
    private float curStockage;

    public float CurStockage
    {
        get { return curStockage; }
        set { curStockage = value; }
    }

    public float CurLife
    {
        get { return curLife; }
        set { curLife = value; }
    }
}
