﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class QueenSave
{
    public float curEnergy;
    public float numberID;

    public GranarySave granary;

    public List<FarmerSave> lst_Farmer;
    public List<NurseSave> lst_Nurse;
    public List<SoldierSave> lst_Soldier;

    public QueenSave()
    {
        this.lst_Farmer = new List<FarmerSave> { };
        this.lst_Nurse = new List<NurseSave> { };
        this.lst_Soldier = new List<SoldierSave> { };
    }
}
