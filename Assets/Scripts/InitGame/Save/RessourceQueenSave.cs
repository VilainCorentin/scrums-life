﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class RessourceQueenSave
{
    public float totalFood;

    public float totalPop;

    public float totalNurse;

    public float totalFarmer;

    public float totalEgg;

    public float totalSoldier;
}
