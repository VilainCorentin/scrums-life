﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class FarmerSave : ScrumSave
{
    public float puitPosX;
    public float puitPosY;
    public float puitPosZ;

    public int nbDiffPuit;
}
