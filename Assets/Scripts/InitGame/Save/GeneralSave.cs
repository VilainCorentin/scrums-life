﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class GeneralSave
{
    public List<DQueenBehavior> lst_allQueen;

    public DPlayerSpec player;

    public float currentIdentity;
    // La food aussi

    public GeneralSave()
    {
        this.lst_allQueen = new List<DQueenBehavior> { };

        this.player = null;
        this.currentIdentity = 0f;
    }
}
