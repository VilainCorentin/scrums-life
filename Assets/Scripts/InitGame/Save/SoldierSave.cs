﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class SoldierSave : ScrumSave 
{
    public bool isLeader;

    public float pointToDefendX;
    public float pointToDefendY;
    public float pointToDefendZ;
}
