﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class ScrumSave
{
    public int typeObjective;
    public float curEnergy;
    public float curLife;
    public float curStockage;

    public float x;
    public float y;
    public float z;
}
