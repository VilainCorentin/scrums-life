﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConfigPutBase : MonoBehaviour {

    //Number of base
    public int nbBase;

    //Contain base and position
    public GameObject[] tabBase = new GameObject[3];
    public Vector3[] tabPosBase;
    public Quaternion[] tabRotBase;
    public Vector3[] tabPosQueen;

    //TODO: METTRE UN QUEEN MOVE POUR PLACE LA REINE AU BON ENDROIT SELON LA BASE

    void Awake()
    {
        this.nbBase = tabBase.Length;
        
        //Init position for Base
        this.tabPosBase = new Vector3[nbBase];
        this.tabPosQueen = new Vector3[nbBase];
        this.tabRotBase = new Quaternion[nbBase];

        this.tabPosBase[0] = new Vector3(246f, 0, 120f);
        this.tabRotBase[0] = this.tabBase[0].gameObject.transform.rotation;

        this.tabPosBase[1] = new Vector3(308f, 0, 378.3f);
        this.tabRotBase[1] = this.tabBase[1].gameObject.transform.rotation;

        this.tabPosBase[2] = new Vector3(612f, 0, 323f);
        this.tabRotBase[2] = this.tabBase[2].gameObject.transform.rotation;

        GameObject.Find("Sun").GetComponent<InitGame>().InitGameNow();
    }


}
