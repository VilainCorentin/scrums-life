﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class InitGame : MonoBehaviour
{

    public ConfigPutBase config;
    public GameObject queen;

    public List<GameObject> lst_allQueen;

    public int numberOfReady;

    public float currentIdentify;

    public LoadGame loadGame;
    public SaveGame saveGame;

    public void InitGameNow()
    {
        this.saveGame = new SaveGame();
        this.loadGame = GameObject.Find("Sun").GetComponent<LoadGame>();

        //Get config
        this.config = GameObject.Find("Sun").GetComponent<ConfigPutBase>();

        this.numberOfReady = 0;

        this.currentIdentify = 0f;

        this.lst_allQueen = new List<GameObject> { };

        for (int i = 0; i < this.config.nbBase; i++)
        {
            this.lst_allQueen.Add(null);

            GameObject base_tmp = Instantiate(this.config.tabBase[i], this.config.tabPosBase[i], this.config.tabRotBase[i]) as GameObject;

            Vector3 queenPos = base_tmp.transform.Find("QueenPos").transform.position;
            Quaternion queenRot = base_tmp.transform.Find("QueenPos").transform.rotation;

            Vector3 posGranary = base_tmp.transform.Find("GranaryPos").transform.position;
            Vector3 posNursery = base_tmp.transform.Find("NurseryPos").transform.position;

            this.lst_allQueen[i] = Instantiate(queen, new Vector3(queenPos.x, queenPos.y, queenPos.z), queenRot);

            if (i == 0)
            {
                this.lst_allQueen[i].GetComponent<QueenBehaviour>().ressource = new RessourceQueenPlayer();
            }
            else
            {
                this.lst_allQueen[i].GetComponent<QueenBehaviour>().ressource = new RessourceQueen();
            }

            this.lst_allQueen[i].GetComponent<QueenBehaviour>().ressource.InitRessource();
            this.lst_allQueen[i].GetComponent<QueenBehaviour>().RefreshRessourceSave();

            this.lst_allQueen[i].GetComponent<QueenBehaviour>().SpawnGranary(posGranary);
            this.lst_allQueen[i].GetComponent<QueenBehaviour>().SpawnNursery(posNursery);

        }
    }

    public void ReadyForLoad()
    {
        this.numberOfReady = this.numberOfReady + 1;

        Debug.Log(Application.persistentDataPath + "/gamesave.save");

        if(this.numberOfReady == (this.lst_allQueen.Count + 1))
        {
            if (File.Exists(Application.persistentDataPath + "/gamesave.save"))
            {
                this.loadGame.Load(lst_allQueen);
            }
        }
    }

    public float GetIdentify()
    {
        this.currentIdentify = this.currentIdentify + 0.1f;
        return this.currentIdentify;
    }

    public void Save()
    {
        this.saveGame.Save(this.lst_allQueen);
    }

}
