﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitGrassDetail : MonoBehaviour {

    public Terrain terrain;

    private int myTextureLayer = 1; //Assumed to be the first non-base texture layer - this is the ground texture from which we wish to sprout grass
    private int myDetailLayer = 0; //Assumed to be the first detail layer - this is the grass we wish to auto-populate

    // Use this for initialization
    void Start()
    {

        float detailCountPerDetailPixel;

        TerrainData terrainData = terrain.terrainData;

        int alphamapWidth = terrain.terrainData.alphamapWidth;
        int alphamapHeight = terrain.terrainData.alphamapHeight;
        int detailWidth = terrain.terrainData.detailResolution;
        int detailHeight = detailWidth;

        float resolutionDiffFactor = (float)alphamapWidth / detailWidth;


        float[,,] splatmap = terrain.terrainData.GetAlphamaps(0, 0, alphamapWidth, alphamapHeight);


        int[,] newDetailLayer = new int[detailWidth, detailHeight];

        //find where the texture is present
        for (int j = 0; j < detailWidth; j++)
        {

            for (int k = 0; k < detailHeight; k++)
            {

                float alphaValue = splatmap[(int)(resolutionDiffFactor * j), (int)(resolutionDiffFactor * k), 0];

                detailCountPerDetailPixel = 0;

                newDetailLayer[j, k] = (int)Mathf.Round(alphaValue * ((float)detailCountPerDetailPixel)) + newDetailLayer[j, k];

            }

        }

        

        terrain.terrainData.SetDetailLayer(0, 0, 0, newDetailLayer);
    }

}
